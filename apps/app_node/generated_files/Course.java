public class Course{ 
	String name; 
	String description; 
	Professor professor; 
	String location; 
	List<Time> times; 
	List<Course> prerequisites; 
	List<Student> students; 
}