var fs = require('fs');

exports.renderIndex = function(req, res, next) {
	res.render('index', { title: 'Java Parser', subtitle: "FJ2CD"});
}

exports.renderParser = function(req,res,next){
	res.render('parser', { title: 'Java Parser', subtitle: "FJ2CD"});
}


exports.handle_parse = function(req,res,next){
	//console.log(req);

	var nodes = JSON.parse(req.body.nodes);
	var link =  JSON.parse(req.body.link);
	var classes = [];
	var names = [];
	for(var i =0; i<nodes.length; i++){
		classes.push(parseToClass(nodes[i], link, nodes));
		names.push(nodes[i].name);
	}

	//res.render('parse_result', {title: 'Java Parser', subtitle: "FJ2CD", classes: classes});
	res.send({classes, names});
	//console.log(classes)
}


parseToClass = function(node, link, nodes){
	var result = link.filter(function(v) {return v.from === node.key;})[0];
	var relation = "";
	if(result !=undefined){
		console.log(result)	
		if (result.relationship == "generalization"){
			var result2 = nodes.filter(function(v) {return v.key === result.to;})[0];
			relation = result2.name
		}
	}
	str = "";
	if(relation == ""){
		str += "public class "+node.name+"{ \n";	
	}else{
		str += "public class "+node.name+" extends "+ relation +"{ \n";	
	}
	
	if(node.properties != undefined){
		for(var j = 0 ; j<node.properties.length; j++){
			str+="\t"
			if(node.properties[j].visibility !== undefined) str += node.properties[j].visibility + " "
			if(node.properties[j].type != undefined) str += node.properties[j].type + " "			
			if(node.properties[j].name != undefined) str += node.properties[j].name+"; \n"
		}
	}

	if(node.methods != undefined){
		for(var j = 0; j < node.methods.length; j++){
			str+= "\t"
			if(node.methods[j].visibility != undefined) str += node.methods[j].visibility + " "
			if(node.methods[j].type != undefined && node.methods[j].type != "void") str += node.methods[j].type + " "
			if(node.methods[j].type == undefined || node.methods[j].type == "void") str += "void" + " "
			if(node.methods[j].name) str += node.methods[j].name + "("
			if(node.methods[j].parameters != undefined){
				for(var k = 0; k < node.methods[j].parameters.length; k++){
					str += node.methods[j].parameters[k].type+" "
					if(k == node.methods[j].parameters.length-1){
						str += node.methods[j].parameters[k].name	
					}else{
						str += node.methods[j].parameters[k].name+","
					}
					
				}
			}
			str += ")"
			if(node.methods[j].type != undefined && node.methods[j].type != "void") str += "{\n\t\treturn null;\n\t} \n"
			else str+= "{}\n"
		}
	}
	str += "}"
	//console.log(str)

	fs.writeFile("./generated_files/"+node.name+'.java', str, function (err) {
		if (err) throw err;
		console.log('It\'s saved!');
	});
	return str;
}
