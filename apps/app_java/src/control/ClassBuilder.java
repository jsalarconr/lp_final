/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import model.ClassToParse;
import model.Member;
import model.Method;
import model.Param;
/**
 *
 * @author sebas
 */
public class ClassBuilder extends JavaBaseListener{
    private ClassToParse classBuilt;

    public ClassBuilder (){
        classBuilt = new ClassToParse();
    }

    ClassBuilder(ClassToParse classBuilt) {
        this.classBuilt = classBuilt;
    }

    @Override
    public void enterTypeDeclaration(JavaParser.TypeDeclarationContext ctx) {
        super.enterTypeDeclaration(ctx);
        classBuilt.setVisibility(ctx.classOrInterfaceModifier(0).getText());
    }

    @Override
    public void enterClassDeclaration(JavaParser.ClassDeclarationContext ctx) {
        super.enterClassDeclaration(ctx);
        classBuilt.setIdentifier(ctx.Identifier().getSymbol().getText());
        classBuilt.setType("Class");
        if(ctx.getChild(2).getText().equals("extends")){
            if(ctx.type().primitiveType() != null){
                classBuilt.getRelations().add(ctx.type().primitiveType().getText());
            }else if(ctx.type().classOrInterfaceType()!= null){
                classBuilt.getRelations().add(ctx.type().classOrInterfaceType().getText());
            }
        }else if(ctx.getChild(2).getText().equals("implements")){
            int size =  ctx.typeList().type().size();
            for (int i = 0; i < size; i++) {
                classBuilt.getRelations().add(ctx.typeList().type(i).getText());
            }
        }
    }

    @Override
    public void enterInterfaceDeclaration(JavaParser.InterfaceDeclarationContext ctx) {
        super.enterInterfaceDeclaration(ctx); //To change body of generated methods, choose Tools | Templates.
        classBuilt.setIdentifier(ctx.Identifier().getSymbol().getText());
        classBuilt.setType("Interface");
    }   
    
    

    @Override
    public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        super.enterMethodDeclaration(ctx); //To change body of generated methods, choose Tools | Templates.
        Method method = new Method();
        Param param;
        method.setIdentifier(ctx.Identifier().getText());
        if (ctx.formalParameters()!=null){
            if (ctx.formalParameters().formalParameterList()!=null){
                if (ctx.formalParameters().formalParameterList().formalParameter()!=null){
                    int size = ctx.formalParameters().formalParameterList().formalParameter().size();
                    for (int i = 0; i < size; i++) {
                        param = new Param();
                        param.setIdentifier(ctx.formalParameters().formalParameterList().formalParameter(i).variableDeclaratorId().getText());
                        if (ctx.formalParameters().formalParameterList().formalParameter(i).type().classOrInterfaceType() != null){
                            param.setType(ctx.formalParameters().formalParameterList().formalParameter(i).type().classOrInterfaceType().getText());
                        }else if(ctx.formalParameters().formalParameterList().formalParameter(i).type().primitiveType()!= null){
                            param.setType(ctx.formalParameters().formalParameterList().formalParameter(i).type().primitiveType().getText());
                        }
                        method.getParams().add(param);
                    }
                }
            }
        }
        if(ctx.type()== null){
            method.setReturnType("void");
        }else  if(ctx.type().primitiveType() != null){
            method.setReturnType(ctx.type().primitiveType().getText());
        }else if(ctx.type().classOrInterfaceType()!= null){
            method.setReturnType(ctx.type().classOrInterfaceType().getText());
        }
        
        classBuilt.getMethods().add(method);
    }

    @Override
    public void enterMemberDeclaration(JavaParser.MemberDeclarationContext ctx) {
        super.enterMemberDeclaration(ctx); //To change body of generated methods, choose Tools | Templates.
        if(ctx.fieldDeclaration() != null){
            Member member;
            int size = ctx.fieldDeclaration().variableDeclarators().variableDeclarator().size();
            for (int i = 0; i < size; i++) {
                member = new Member();
                String identifier = ctx.fieldDeclaration().variableDeclarators().variableDeclarator(i).variableDeclaratorId().Identifier().getText();
                member.setIdentifier(identifier);
                if(ctx.fieldDeclaration().type().primitiveType() != null){
                    member.setType(ctx.fieldDeclaration().type().primitiveType().getText());
                }else if(ctx.fieldDeclaration().type().classOrInterfaceType()!= null){
                    member.setType(ctx.fieldDeclaration().type().classOrInterfaceType().getText());
                }
                classBuilt.getMembers().add(member);
            }
        }
    }

    @Override
    public void enterInterfaceMethodDeclaration(JavaParser.InterfaceMethodDeclarationContext ctx) {
        super.enterInterfaceMethodDeclaration(ctx); //To change body of generated methods, choose Tools | Templates.
        
        Method method = new Method();
        Param param;
        method.setIdentifier(ctx.Identifier().getText());
        if (ctx.formalParameters()!=null){
            if(ctx.formalParameters().formalParameterList()!=null){
                int size = ctx.formalParameters().formalParameterList().formalParameter().size();
                for (int i = 0; i < size; i++) {
                    param = new Param();
                    param.setIdentifier(ctx.formalParameters().formalParameterList().formalParameter(i).variableDeclaratorId().getText());
                    if (ctx.formalParameters().formalParameterList().formalParameter(i).type().classOrInterfaceType() != null){
                        param.setType(ctx.formalParameters().formalParameterList().formalParameter(i).type().classOrInterfaceType().getText());
                    }else if(ctx.formalParameters().formalParameterList().formalParameter(i).type().primitiveType()!= null){
                        param.setType(ctx.formalParameters().formalParameterList().formalParameter(i).type().primitiveType().getText());
                    }
                    method.getParams().add(param);
                }
            }
        }
        
        //int size = ctx.formalParameters().formalParameterList().formalParameter().size();
        
        if(ctx.type()== null){
            method.setReturnType("void");
        }else if(ctx.type().primitiveType() != null){
            method.setReturnType(ctx.type().primitiveType().getText());
        }else if(ctx.type().classOrInterfaceType()!= null){
            method.setReturnType(ctx.type().classOrInterfaceType().getText());
        }
        
        classBuilt.getMethods().add(method);
    }   
}