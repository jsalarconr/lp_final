// import de librerias de runtime de ANTLR
package control;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import model.ClassToParse;
public class Analyzer {
    
    public ClassToParse analyze(String path){
        try {
            ClassToParse classBuilt = new ClassToParse();
            // crear un analizador léxico que se alimenta a partir de la entrada (archivo o consola)
            JavaLexer lexer;         
            lexer = new JavaLexer(new ANTLRFileStream(path));
            // Identificar al analizador léxico como fuente de tokens para el sintactico
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            // Crear el analizador sintáctico que se alimenta a partir del buffer de tokens
            JavaParser parser = new JavaParser(tokens);
            ParseTree tree = parser.compilationUnit(); // comienza el análisis en la regla inicial
            //System.out.println(tree.toStringTree(parser)); // imprime el árbol en forma textual
            // Create a generic parse tree walker that can trigger callbacks
            ParseTreeWalker walker = new ParseTreeWalker();
            // Walk the tree created during the parse, trigger callbacks
            walker.walk(new ClassBuilder(classBuilt), tree);
            //System.out.println(); // print a \n after translation
            return classBuilt;
        } catch (Exception e) {
            System.err.println("Error (Test): " + e);
            return null;
        }
    }
}
