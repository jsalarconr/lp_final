/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;
import java.util.HashMap;
import model.ClassToParse;
import model.Member;
import model.Method;
import model.Param;

/**
 *
 * @author sebas
 */
public class ClassParserToJS {

    ArrayList<ClassToParse> classList;
    HashMap<String, Integer> map;
    private String output;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
    
    public ClassParserToJS(ArrayList<ClassToParse> classList) {
        map = new HashMap<>();
        this.classList = classList;
        this.output = "   function init() {\n" +
        "    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this\n" +
        "    var $ = go.GraphObject.make;\n" +
        "    myDiagram =\n" +
        "      $(go.Diagram, \"myDiagram\",\n" +
        "        {\n" +
        "          initialContentAlignment: go.Spot.Center,\n" +
        "          \"undoManager.isEnabled\": true,\n" +
        "          layout: $(go.TreeLayout,\n" +
        "                    { // this only lays out in trees nodes connected by \"generalization\" links\n" +
        "                      angle: 90,\n" +
        "                      path: go.TreeLayout.PathSource,  // links go from child to parent\n" +
        "                      setsPortSpot: false,  // keep Spot.AllSides for link connection spot\n" +
        "                      setsChildPortSpot: false,  // keep Spot.AllSides\n" +
        "                      // nodes not connected by \"generalization\" links are laid out horizontally\n" +
        "                      arrangement: go.TreeLayout.ArrangementHorizontal\n" +
        "                    })\n" +
        "        });\n" +
        "    // show visibility or access as a single character at the beginning of each property or method\n" +
        "    function convertVisibility(v) {\n" +
        "      switch (v) {\n" +
        "        case \"public\": return \"+\";\n" +
        "        case \"private\": return \"-\";\n" +
        "        case \"protected\": return \"#\";\n" +
        "        case \"package\": return \"~\";\n" +
        "        default: return v;\n" +
        "      }\n" +
        "    }\n" +
        "    // the item template for properties\n" +
        "    var propertyTemplate =\n" +
        "      $(go.Panel, \"Horizontal\",\n" +
        "        // property visibility/access\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: false, width: 12 },\n" +
        "          new go.Binding(\"text\", \"visibility\", convertVisibility)),\n" +
        "        // property name, underlined if scope==\"class\" to indicate static property\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: true },\n" +
        "          new go.Binding(\"text\", \"name\").makeTwoWay(),\n" +
        "          new go.Binding(\"isUnderline\", \"scope\", function(s) { return s[0] === 'c' })),\n" +
        "        // property type, if known\n" +
        "        $(go.TextBlock, \"\",\n" +
        "          new go.Binding(\"text\", \"type\", function(t) { return (t ? \": \" : \"\"); })),\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: true },\n" +
        "          new go.Binding(\"text\", \"type\").makeTwoWay()),\n" +
        "        // property default value, if any\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: false },\n" +
        "          new go.Binding(\"text\", \"default\", function(s) { return s ? \" = \" + s : \"\"; }))\n" +
        "      );\n" +
        "    // the item template for methods\n" +
        "    var methodTemplate =\n" +
        "      $(go.Panel, \"Horizontal\",\n" +
        "        // method visibility/access\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: false, width: 12 },\n" +
        "          new go.Binding(\"text\", \"visibility\", convertVisibility)),\n" +
        "        // method name, underlined if scope==\"class\" to indicate static method\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: true },\n" +
        "          new go.Binding(\"text\", \"name\").makeTwoWay(),\n" +
        "          new go.Binding(\"isUnderline\", \"scope\", function(s) { return s[0] === 'c' })),\n" +
        "        // method parameters\n" +
        "        $(go.TextBlock, \"()\",\n" +
        "          // this does not permit adding/editing/removing of parameters via inplace edits\n" +
        "          new go.Binding(\"text\", \"parameters\", function(parr) {\n" +
        "              var s = \"(\";\n" +
        "              for (var i = 0; i < parr.length; i++) {\n" +
        "                var param = parr[i];\n" +
        "                if (i > 0) s += \", \";\n" +
        "                s += param.name + \": \" + param.type;\n" +
        "              }\n" +
        "              return s + \")\";\n" +
        "          })),\n" +
        "        // method return type, if any\n" +
        "        $(go.TextBlock, \"\",\n" +
        "          new go.Binding(\"text\", \"type\", function(t) { return (t ? \": \" : \"\"); })),\n" +
        "        $(go.TextBlock,\n" +
        "          { isMultiline: false, editable: true },\n" +
        "          new go.Binding(\"text\", \"type\").makeTwoWay())\n" +
        "      );\n" +
        "    // this simple template does not have any buttons to permit adding or\n" +
        "    // removing properties or methods, but it could!\n" +
        "    myDiagram.nodeTemplate =\n" +
        "      $(go.Node, \"Auto\",\n" +
        "        {\n" +
        "          locationSpot: go.Spot.Center,\n" +
        "          fromSpot: go.Spot.AllSides,\n" +
        "          toSpot: go.Spot.AllSides\n" +
        "        },\n" +
        "        $(go.Shape, { fill: \"lightyellow\" }),\n" +
        "        $(go.Panel, \"Table\",\n" +
        "          { defaultRowSeparatorStroke: \"black\" },\n" +
        "          // header\n" +
        "          $(go.TextBlock,\n" +
        "            {\n" +
        "              row: 0, margin: 3, alignment: go.Spot.Center,\n" +
        "              font: \"bold 12pt sans-serif\",\n" +
        "              isMultiline: false, editable: true\n" +
        "            },\n" +
        "            new go.Binding(\"text\", \"name\").makeTwoWay()),\n" +
        "          // properties\n" +
        "          $(go.Panel, \"Vertical\",\n" +
        "            new go.Binding(\"itemArray\", \"properties\"),\n" +
        "            {\n" +
        "              row: 1, margin: 3, alignment: go.Spot.Left,\n" +
        "              defaultAlignment: go.Spot.Left,\n" +
        "              itemTemplate: propertyTemplate\n" +
        "            }\n" +
        "          ),\n" +
        "          // methods\n" +
        "          $(go.Panel, \"Vertical\",\n" +
        "            new go.Binding(\"itemArray\", \"methods\"),\n" +
        "            {\n" +
        "              row: 2, margin: 3, alignment: go.Spot.Left,\n" +
        "              defaultAlignment: go.Spot.Left,\n" +
        "              itemTemplate: methodTemplate\n" +
        "            }\n" +
        "          ))\n" +
        "      );\n" +
        "    function convertIsTreeLink(r) {\n" +
        "      return r === \"generalization\";\n" +
        "    }\n" +
        "    function convertFromArrow(r) {\n" +
        "      switch (r) {\n" +
        "        case \"generalization\": return \"\";\n" +
        "        default: return \"\";\n" +
        "      }\n" +
        "    }\n" +
        "    function convertToArrow(r) {\n" +
        "      switch (r) {\n" +
        "        case \"generalization\": return \"Triangle\";\n" +
        "        case \"aggregation\": return \"StretchedDiamond\";\n" +
        "        default: return \"\";\n" +
        "      }\n" +
        "    }\n" +
        "    myDiagram.linkTemplate =\n" +
        "      $(go.Link,\n" +
        "        { routing: go.Link.Orthogonal },\n" +
        "        new go.Binding(\"isLayoutPositioned\", \"relationship\", convertIsTreeLink),\n" +
        "        $(go.Shape),\n" +
        "        $(go.Shape, { scale: 1.3, fill: \"white\" },\n" +
        "          new go.Binding(\"fromArrow\", \"relationship\", convertFromArrow)),\n" +
        "        $(go.Shape, { scale: 1.3, fill: \"white\" },\n" +
        "          new go.Binding(\"toArrow\", \"relationship\", convertToArrow))\n" +
        "      );\n" +
        "    // setup a few example class nodes and relationships\n" +
        "    var nodedata = [\n"; 
        int index = 1;
        for (ClassToParse classToParse : classList) {
            parser(classToParse, index++);
        }
        this.output += "    ];\n";
        this.output += "var linkdata=[\n";
        for (ClassToParse classToParse : classList) {
            buildLink(classToParse);
        }
        this.output += "];\n";
        this.output += " myDiagram.model = $(go.GraphLinksModel,\n" +
"      {\n" +
"        copiesArrays: true,\n" +
"        copiesArrayObjects: true,\n" +
"        nodeDataArray: nodedata,\n" +
"        linkDataArray: linkdata\n" +
"      });\n" +
"  }";
    }
    
    public void parser(ClassToParse classToParse, int key){
        map.put(classToParse.getIdentifier(), key);
        ArrayList<Member> members = classToParse.getMembers();
        ArrayList<Method> methods = classToParse.getMethods();
        if("Class".equals(classToParse.getType())){
            this.output += "        {\n"
                + "            key: "+key+",\n"
                + "            name: \""+classToParse.getIdentifier()+"\", \n";
        }else if("Interface".equals(classToParse.getType())){
            this.output += "        {\n"
                + "            key: "+key+",\n"
                + "            name: \"<<interface>>"+classToParse.getIdentifier()+"\", \n";
        }
        
        if(members.size()>0){
            this.output+="            properties:[\n";
            for (Member member : members) {
                this.output += "                {"
                        + "name: \""+member.getIdentifier()+"\","
                        + "type: \""+member.getType()+"\""
                        + "},\n";
            }
            this.output+= "            ],\n";
        }
        
        if(methods.size()>0){
            this.output+= "            methods: [\n";
            for (Method method : methods) {
                this.output += "                {"
                    + "name: \"" +method.getIdentifier()+"\","
                    + "type: \"" +method.getReturnType()+"\",";
                
                ArrayList<Param> params = method.getParams();
                if(params.size()>0){
                    this.output += "parameters: [";
                    for (Param param : params) {
                        this.output+="{"
                                +"name: \""+param.getIdentifier()+"\","
                                +"type: \""+param.getType()+"\""
                                + "}, ";
                    }
                    this.output += "]";
                }
                this.output += "},\n";
            }
            this.output += "]\n";
        }
                
        //this.output += "            ]\n"; 
        this.output+= "        },\n";
    }

    private void buildLink(ClassToParse classToParse) {
        ArrayList<String> relations = classToParse.getRelations();
        if(relations.size() >0){
            
            for (String relation : relations) {
                if(map.get(relation) != null){
                    this.output+= "        { "
                        + "from:" +map.get(classToParse.getIdentifier()) +","
                        + "to: "+map.get(relation)+"," 
                        + "relationship: \"generalization\" },\n";
                }
            }   
        }
    }
}
