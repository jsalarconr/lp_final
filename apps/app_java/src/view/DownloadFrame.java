package view;

import control.ClassParserToJS;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class DownloadFrame extends javax.swing.JFrame {

    private final ClassParserToJS parsertText;

    /**
     * Creates new form DownloadFrame
     */
    public DownloadFrame(ClassParserToJS parsertText) {
        initComponents();
        this.parsertText = parsertText;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        generateButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("DejaVu Sans", 1, 22)); // NOI18N
        jLabel1.setText("Java Parser: FJ2CD");

        jLabel2.setText("<html>Thank you for using this software. You have to download<br> the generated file, the library and the .html file. <br><br> You have to put them in the same location.");

        generateButton.setText("Download generated files");
        generateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateButtonActionPerformed(evt);
            }
        });

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(110, 110, 110)
                        .addComponent(generateButton)))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(generateButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void generateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateButtonActionPerformed
        chooser = new JFileChooser(); 
        chooser.setCurrentDirectory(new java.io.File(""));
        chooser.setDialogTitle("Select the folder to save the output file");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
            String path = chooser.getSelectedFile().toString();
            OutputStream output = null;
            InputStream input = null;
            
            /** Library Saving **/
            
            File sourceFileLibrary = new File(getClass().getResource("/resources/go.js").getFile());
            File destFileLibrary = new File(path+"/go.js");
            
            //OutputStream output = null;

            try {
                destFileLibrary.createNewFile();
		/* FileInputStream to read streams */
		input = new FileInputStream(sourceFileLibrary);
		/* FileOutputStream to write streams */
		output = new FileOutputStream(destFileLibrary);
		byte[] buf = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buf)) > 0) {
                    output.write(buf, 0, bytesRead);
		}

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }finally {
		try {
                    if (null != input) {
			input.close();
                    }
				
                    if (null != output) {
			output.close();
                    }
                    
                    //JOptionPane.showMessageDialog(this, "Library saved. . Remember to put the three files together in same location");

		} catch (IOException e) {
		}
            }
            
            
            /** -- Index.html **/
            
            File sourceFileIndex = new File(getClass().getResource("/resources/index.html").getFile());
            File destFileIndex = new File(path+"/index.html");
            
            
            try {
                destFileIndex.createNewFile();
		/* FileInputStream to read streams */
		input = new FileInputStream(sourceFileIndex);
		/* FileOutputStream to write streams */
		output = new FileOutputStream(destFileIndex);
		byte[] buf = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buf)) > 0) {
                    output.write(buf, 0, bytesRead);
		}

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }finally {
		try {
                    if (null != input) {
			input.close();
                    }
				
                    if (null != output) {
			output.close();
                    }
                    
                    //JOptionPane.showMessageDialog(this, "Template saved. Remember to put the three files together in same location");

		} catch (IOException e) {
		}
            }
            
            
            /** -- Generated File --**/
            
            File destFile = new File(path+"/classDiagram.js");
            try {
                destFile.createNewFile();
		/* FileOutputStream to write streams */
		output = new FileOutputStream(destFile);

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }finally {
		try {
                    byte[] contentInBytes = parsertText.getOutput().getBytes();

                    output.write(contentInBytes);

                    if (null != output) {
                        output.flush();
			output.close();
                    }
                    JOptionPane.showMessageDialog(this, "Output file saved. . Remember to put the three files together in same location");
		} catch (IOException e) {
                    e.printStackTrace();
		}
            }
        }
    }//GEN-LAST:event_generateButtonActionPerformed

    
    JFileChooser chooser;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton generateButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    // End of variables declaration//GEN-END:variables
}
