package control;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author sebas
 */
public class TwoOutTwoImage {
    
    private int[][] matrixShare1;
    private int[][] matrixShare2;
    private int[][] matrixImage;
    private String outputFilePath;
    private String share1Path;
    private String share2Path;
    private String decryptedPath;

    public String getDecryptedPath() {
        return decryptedPath;
    }

    public void setDecryptedPath(String decryptedPath) {
        this.decryptedPath = decryptedPath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public String getShare1Path() {
        return share1Path;
    }

    public void setShare1Path(String share1Path) {
        this.share1Path = share1Path;
    }

    public String getShare2Path() {
        return share2Path;
    }

    public void setShare2Path(String share2Path) {
        this.share2Path = share2Path;
    }
    
    
    public void writeImage(int[][] matrix, String name) throws IOException{
        BufferedImage image = new BufferedImage(matrix.length, matrix[0].length, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                Color colorin = new Color(matrix[x][y], matrix[x][y], matrix[x][y]);
                image.setRGB(x, y, colorin.getRGB());
            }
        }
        File outputfile = new File("src/images/"+name+".png");
        ImageIO.write(image, "png", outputfile);
    }
    
    public void encrypt(String inputPath, String outputPath, String share1Path, String share2Path) throws IOException{
        BufferedImage input = ImageIO.read(getClass().getResourceAsStream("/images/"+inputPath));
        int weight = input.getWidth();
        int height = input.getHeight();
        int[][] image = new int[height][weight];
        int size=2;
        int[][] share1 = new int[image.length*size][image[0].length*size];
        int[][] share2 = new int[image.length*size][image[0].length*size];
        BufferedImage bufferShare1 = new BufferedImage(share1[0].length, share1.length,BufferedImage.TYPE_INT_RGB);
        BufferedImage bufferShare2 = new BufferedImage(share2[0].length, share2.length,BufferedImage.TYPE_INT_RGB);
        
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < weight; x++) {
                Color c = new Color(input.getRGB(x, y));
                int grayLevel = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
                if(grayLevel<128) grayLevel=0;
                else grayLevel=255;
                image[y][x] = grayLevel;
                this.encryptShares(y, x, size, grayLevel, share1, share2, bufferShare1, bufferShare2);
                c=new Color(grayLevel,grayLevel,grayLevel);
                input.setRGB(x, y, c.getRGB());
            }
        }
        this.matrixShare1=share1;
        this.matrixShare2=share2;
        this.matrixImage=image;
        File outputfile = File.createTempFile(outputPath, ".png"); 
        //File outputfile = new File("/"+outputPath+".png");
        this.outputFilePath = outputfile.getAbsolutePath();
        ImageIO.write(input, "png", outputfile);
        outputfile= File.createTempFile(share1Path, ".png");
        ImageIO.write(bufferShare1, "png", outputfile);
        this.share1Path = outputfile.getAbsolutePath();
        outputfile= File.createTempFile(share2Path,".png");
        ImageIO.write(bufferShare2, "png", outputfile);
        this.share2Path = outputfile.getAbsolutePath();
    }
    
    private void encryptShares(int i, int j, int size, int value, int[][] share1, int[][] share2, BufferedImage buffer1, BufferedImage buffer2){
        int x=i*size; int y= j*size;
        int random=(int)Math.round(Math.random()*5);
        switch(random){
            case 0:
                share1[x][y]=0;
                share1[x][y+1]=255;
                share1[x+1][y]=0;
                share1[x+1][y+1]=255;
                if(value==0){
                    share2[x][y]=255;
                    share2[x][y+1]=0;
                    share2[x+1][y]=255;
                    share2[x+1][y+1]=0;
                }else{
                    share2[x][y]=0;
                    share2[x][y+1]=255;
                    share2[x+1][y]=0;
                    share2[x+1][y+1]=255;
                }
                break;
            case 1:
                share1[x][y]=255;
                share1[x][y+1]=0;
                share1[x+1][y]=255;
                share1[x+1][y+1]=0;
                if(value==0){
                    share2[x][y]=0;
                    share2[x][y+1]=255;
                    share2[x+1][y]=0;
                    share2[x+1][y+1]=255;
                }else{
                    share2[x][y]=255;
                    share2[x][y+1]=0;
                    share2[x+1][y]=255;
                    share2[x+1][y+1]=0;
                }
                break;
            case 2:
                share1[x][y]=0;
                share1[x][y+1]=0;
                share1[x+1][y]=255;
                share1[x+1][y+1]=255;
                if(value==0){
                    share2[x][y]=255;
                    share2[x][y+1]=255;
                    share2[x+1][y]=0;
                    share2[x+1][y+1]=0;
                }else{
                    share2[x][y]=0;
                    share2[x][y+1]=0;
                    share2[x+1][y]=255;
                    share2[x+1][y+1]=255;
                }
                break;
            case 3:
                share1[x][y]=255;
                share1[x][y+1]=255;
                share1[x+1][y]=0;
                share1[x+1][y+1]=0;
                if(value==0){
                    share2[x][y]=0;
                    share2[x][y+1]=0;
                    share2[x+1][y]=255;
                    share2[x+1][y+1]=255;
                }else{
                    share2[x][y]=255;
                    share2[x][y+1]=255;
                    share2[x+1][y]=0;
                    share2[x+1][y+1]=0;
                }
                break;
            case 4:
                share1[x][y]=0;
                share1[x][y+1]=255;
                share1[x+1][y]=255;
                share1[x+1][y+1]=0;
                if(value==0){
                    share2[x][y]=255;
                    share2[x][y+1]=0;
                    share2[x+1][y]=0;
                    share2[x+1][y+1]=255;
                }else{
                    share2[x][y]=0;
                    share2[x][y+1]=255;
                    share2[x+1][y]=255;
                    share2[x+1][y+1]=0;
                }
                break;
            case 5:
                share1[x][y]=255;
                share1[x][y+1]=0;
                share1[x+1][y]=0;
                share1[x+1][y+1]=255;
                if(value==0){
                    share2[x][y]=0;
                    share2[x][y+1]=255;
                    share2[x+1][y]=255;
                    share2[x+1][y+1]=0;
                }else{
                    share2[x][y]=255;
                    share2[x][y+1]=0;
                    share2[x+1][y]=0;
                    share2[x+1][y+1]=255;
                }
                break;
        }
        int col;
        col =share1[x][y];
        buffer1.setRGB(y, x, new Color(col,col,col).getRGB());
        col=share1[x][y+1];
        buffer1.setRGB(y+1, x, new Color(col,col,col).getRGB());
        col=share1[x+1][y];
        buffer1.setRGB(y, x+1, new Color(col,col,col).getRGB());
        col=share1[x+1][y+1];
        buffer1.setRGB(y+1, x+1, new Color(col,col,col).getRGB());
        
        col=share2[x][y];
        buffer2.setRGB(y, x, new Color(col,col,col).getRGB());
        col=share2[x][y+1];
        buffer2.setRGB(y+1, x, new Color(col,col,col).getRGB());
        col=share2[x+1][y];
        buffer2.setRGB(y, x+1, new Color(col,col,col).getRGB());
        col=share2[x+1][y+1];
        buffer2.setRGB(y+1, x+1, new Color(col,col,col).getRGB());
    }
    
    public void decryptImage(String pathShare1, String pathShare2, String outputName) throws IOException{
        File inputFile1 = new File(pathShare1);
        BufferedImage share1 = ImageIO.read(inputFile1);
        File inputFile2 = new File(pathShare2);
        BufferedImage share2 = ImageIO.read(inputFile2);
        int width1 = share1.getWidth();
        int height1 = share1.getHeight();
        BufferedImage outputImage = new BufferedImage(width1, height1, BufferedImage.TYPE_INT_RGB);
        for(int y=0; y<height1; y++){
            for(int x=0; x<width1; x++){
                int c1 = new Color(share1.getRGB(x, y)).getBlue();
                int c2 = new Color(share2.getRGB(x, y)).getBlue();
                int c3;
                if(c1==0||c2==0) c3=0;
                else c3=255;
                outputImage.setRGB(x, y, new Color(c3,c3,c3).getRGB());
            }
        }
        File outputFile = File.createTempFile(outputName,".png");
        ImageIO.write(outputImage, "png", outputFile);
        this.decryptedPath = outputFile.getAbsolutePath();
        
    }
    
}
