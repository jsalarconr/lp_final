package control;

import java.awt.Point;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class GammaPentagonal {
    
    private double xMin, xMax, yMin, yMax;
    byte table[][];
    Point pointAux1 = new Point();
    private HashSet hashSet[][];
    public boolean graph1 = true;

    public byte[][] getTable() {
        return table;
    }

    public void setTable(byte[][] table) {
        this.table = table;
    }
    
    public GammaPentagonal(double xMin, double xMax, double yMin, double yMax) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.table = initializeTable( (int) yMax, (int) xMax);
    }
    
    public LinkedList generatePermutation(int max){
        LinkedList list = new LinkedList();
        for (int i = 0; i < max; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        return list;
      
    }
    
    public byte[][] initializeTable(int row, int column) {
        byte[][] table = new byte[column][row];
        if (!this.graph1) {
            this.hashSet = new HashSet[column][row];
        }
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                table[i][j] = (byte) ( ( (i + j) % 26) + 'a');
            }
        }
        return table;
    }
    
    
    private Point searchCharacter(Point point, byte a) {
        int x = point.x;
        int y = point.y;
        while (x < this.xMax) {
            while (y < this.yMax) {
                if (table[x][y] == a) {
                    return new Point(x, y);
                }
                y++;
                pointAux1.setLocation(x, y);
                if (point.equals(pointAux1)) {
                    return null;
                }
            }
            y = 0;
            if (x + 1 == this.xMax) {
                x = -1;
            }
            x++;
        }
        return null;
    }
    
    public LinkedList crypt(String text) {
        LinkedList list = new LinkedList();
        try {
            Point pointAux, point = new Point(0, 0);
            byte array[] = text.getBytes();
            for (int i = 0; i < array.length; i++) {
                pointAux = searchCharacter(point, array[i]);
                if (pointAux.x + 1 == this.xMax && pointAux.y + 1 == this.yMax) {
                    point = new Point(0, 0);
                }
                else if (pointAux.y + 1 == table[0].length) {
                    point = new Point(pointAux.x + 1, 0);
                }
                else {
                    point = new Point(pointAux.x, pointAux.y + 1);
                }
                list.add(pointAux);
            }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                "insufficient arrangement of characters",
                "Incomplete Encryption",
                JOptionPane.ERROR_MESSAGE);
        }
        return list;
    }
}