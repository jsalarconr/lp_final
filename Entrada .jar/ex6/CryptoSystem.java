package control;

/**
 *
 * @author sebas
 */
public class CryptoSystem {
    private String ciphertext;
    private String plaintext;
    final Uncrypt uncryptctrl;
    
    public String getCiphertext() {
        return ciphertext;
    }

    public void setCiphertext(String ciphertext) {
        this.ciphertext = ciphertext;
    }

    public String getPlaintext() {
        return plaintext;
    }

    public void setPlaintext(String plaintext) {
        this.plaintext = plaintext;
    }
    
    public CryptoSystem(){
        uncryptctrl = new Uncrypt();
    }
}
