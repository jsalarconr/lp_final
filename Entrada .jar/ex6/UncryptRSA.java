package control;

import java.util.ArrayList;
import java.util.Base64;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.StringTokenizer;
import javax.crypto.Cipher;

/**
 *
 * @author sebas
 */
public class UncryptRSA extends CryptoSystem{
    
    Cipher cipher;
    private String key;
    
    public String generateKey() throws Exception{
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        SecureRandom random = new SecureRandom();
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
        generator.initialize(256, random);
        KeyPair pair = generator.generateKeyPair();
        Key pubKey = pair.getPublic();
        Key privKey = pair.getPrivate();
        String encodedPubKey = Base64.getEncoder().encodeToString(pubKey.getEncoded());
        String encodedPrivKey = Base64.getEncoder().encodeToString(privKey.getEncoded());
        String suma = encodedPubKey +"\n\n"+ encodedPrivKey;
        return suma;
    }

    public String checkFields(String message, String key, boolean isEncrypt) {
        return "success";
    }

    public void crypt(String pt, String key) throws Exception{
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        
        this.key=key;
        setPlaintext(pt);
        setCiphertext("");
        SecureRandom random = new SecureRandom();
        StringTokenizer strTkn = new StringTokenizer(key,"\n\n");
        ArrayList<String> keys = new ArrayList<String>(2);

        while(strTkn.hasMoreTokens()){
            keys.add(strTkn.nextToken());
        }
        String pubKey = keys.get(0);
        String privKey = keys.get(1);
        
        byte[] decodedKey = Base64.getDecoder().decode(pubKey);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(decodedKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey pub = keyFactory.generatePublic(spec);
        
        cipher = Cipher.getInstance("RSA/None/NoPadding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, pub, random);
        byte[] utf8 = getPlaintext().getBytes("UTF8");
        // Encrypt
        byte[] enc = cipher.doFinal(utf8);
        setCiphertext(new sun.misc.BASE64Encoder().encode(enc));
    }

    public void decrypt(String ct, String key) throws Exception{
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        this.key=key;
        setCiphertext(ct);
        setPlaintext("");
        
        StringTokenizer strTkn = new StringTokenizer(key,"\n\n");
        ArrayList<String> keys = new ArrayList<String>(2);

        while(strTkn.hasMoreTokens()){
            keys.add(strTkn.nextToken());
        }
        String pubKey = keys.get(0);
        String privKey = keys.get(1);
        
        byte[] decodedKey1 = Base64.getDecoder().decode(privKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedKey1);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey priv = fact.generatePrivate(keySpec);
        
        cipher = Cipher.getInstance("RSA/None/NoPadding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, priv);
        byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(getCiphertext());
        byte[] utf8 = cipher.doFinal(dec);
        // Decode using utf-8
        setPlaintext(new String(utf8, "UTF8"));
    }
}
