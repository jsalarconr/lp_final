package control;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author sebas
 */
public class Uncrypt {
    public static final String[] ALPHABET =  {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    public static final HashMap<Integer, Integer> INVERSE = new HashMap<Integer, Integer>() {
            { 
                put(1, 1); 
                put(3, 9); 
                put(5, 21);
                put(7, 15); 
                put(9, 3); 
                put(11, 19); 
                put(15, 7); 
                put(17, 23);
                put(19, 11); 
                put(23, 17);
                put(25, 25); 
            }
    };
    
    public static final HashMap<String, Double> STANDARDFREQUENCY = new HashMap<String,Double>(){
        {
            put("A",0.082);
            put("B",0.015);
            put("C",0.028);
            put("D",0.043);
            put("E",0.127);
            put("F",0.022);
            put("G",0.020);
            put("H",0.061);
            put("I",0.070);
            put("J",0.002);
            put("K",0.008);
            put("L",0.040);
            put("M",0.024);
            put("N",0.067);
            put("O",0.075);
            put("P",0.019);
            put("Q",0.001);
            put("R",0.060);
            put("S",0.063);
            put("T",0.091);
            put("U",0.028);
            put("V",0.010);
            put("W",0.023);
            put("X",0.001);
            put("Y",0.020);
            put("Z",0.001);
        }  
    };
    
    
    /**
    * Calcula los valores que cumplen la identidad de Bezout
    */
    public static double[] identidad(double x,double y)throws Exception{

       double c,aux;
       double j1,j2,j3,k1,k2,k3,l1,l2,l3;

       if((x<0) || (y<0))
             throw new Exception("Los números deben ser positivos");
       j1=x; j3=y; l1=1; l3=0;  k1=0; k3=1;

        while (j3!=0){
            c=java.lang.Math.floor(j1/j3);
            j2=j1%j3; l2=l1-c*l3; k2=k1-c*k3;
            j1=j3; j3=j2; l1=l3;
            l3=l2; k1=k3; k3=k2;
        }
        double[] res = new double[2];
        res[0]=l1; res[1]=k1;
        return res;
    }
    
    public static boolean primoMR(int n, int t){
        boolean compuesto = true;
        int m=n-1; int s=0;
        int r,a,b;
        while ((m % 2) == 0){
            s=s+1; m=m/2;   // s = veces que 2 divide a n-1
        }
        r=m;
        int i=1;
        while (compuesto && (i<t)){
            a=(int) Math.floor(2+(Math.random()* (n-2))); //a= aleatorio 2..n-1
            if (mcd(a,n)!=1)
                i=t; //acabamos -> es compuesto
            else{
                b=(int) expRapida(a,r,n);       // b = a^r mod n
                if ((b!=1) && (b!=-1) )
                for (int j=0; j<=s-1; j++)
                    b=(int) expRapida(b,2,n);   // b = b^2 mod n
                    compuesto = (b!=1) && (b!=-1);             // sigue sin ser primo
            }
            i++;
        }
        return !compuesto;
    }
    
    /**
    * Halla el maximo comun divisor de dos numeros
    */
    public static double mcd(double x,double y ){
        double a,b;
        a = x;
        b = y;
        if ((a<1) || (b<0))
        return 1;
        else {
            while( a != b ){
                if( a < b ){
                    b = b - a;
                } else {
                    a = a - b;
                }
            }
        return(a);
        }
    }

    /**
    * Realiza la operacion: num^b mod n  de forma eficiente
    */
    public static double expRapida(double num,double b,double n){
        double z, x, resul;
        z = b;
        x = num;
        resul = 1;

        while (z>0){
            if ((z%2)== 1){
                resul = (resul*x) % n;
                z--;
            }else{
                x = (x*x) % n;
                z = java.lang.Math.floor(z/2);
            }
        }
        return resul;
    }
    
    
    /**
   * Realiza la operacion: num^b mod n  de forma eficiente
   * Permite además precision ilimitada, ya que usa una funcion predefinida
   */
    public static double expRapidaJ(int num,int b,int n){
        BigInteger exp = new BigInteger(String.valueOf(b));
        BigInteger modulo = new BigInteger(String.valueOf(n));
        BigInteger numero = new BigInteger(String.valueOf(num));
        BigInteger res = numero.modPow(exp,modulo);
        return res.doubleValue();
    }
    
    public boolean isNumeric(String s) {
        return java.util.regex.Pattern.matches("\\d+", s);
    }
    
    public boolean isAlpha(String s) {
        return s.matches("[a-zA-Z]+");
    }
    
    public ArrayList convertStringToCode(String plaintext){
        ArrayList codes = new ArrayList();
        for(int i =0; i<plaintext.length(); i++){
            codes.add((int)plaintext.charAt(i)-65);
        }
        return codes;
    }
    
    public String convertCodeToString(ArrayList<Integer> codes){
        String text="";
        for (Integer code : codes) {
            
            text += new Character((char) (code + 65)).toString();
        }
        
        return text;
    }
    
    public int mod(int number, int mod){
        return ((number%mod)+mod)%mod;
    }
    
   public int gcd(int a, int b) {
        if (b==0) return a;
        return gcd(b,a%b);
    }
}
