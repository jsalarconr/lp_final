package control;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author sebas
 */
public class PreProcess {
    
    private int lowBoundS1;
    private int upperBoundS1;
    
    private int lowBoundS2;
    private int upperBoundS2;
    
    private int lowBoundT;
    private int upperBoundT;
    
    private double L;
    private final double U = 0.55;
    
    private String outputS1path;
    private String outputS2path;
    private String outputTpath;
    
    
    public String getOutputS1path() {
        return outputS1path;
    }

    public void setOutputS1path(String outoutS1path) {
        this.outputS1path = outoutS1path;
    }

    public String getOutputS2path() {
        return outputS2path;
    }

    public void setOutputS2path(String outputS2path) {
        this.outputS2path = outputS2path;
    }

    public String getOutputTpath() {
        return outputTpath;
    }

    public void setOutputTpath(String outputTpath) {
        this.outputTpath = outputTpath;
    }
    
    public PreProcess(){
        double aux = Math.max(0, 2*this.U-1);
        this.L=aux+(this.U-aux)/2;
    }
    
    private Point findRange(BufferedImage image){
        int max=0;
        int min=255;
        
        for(int x=0; x<image.getWidth(); x++){
            for(int y=0; y <image.getHeight(); y++){
                Color c=new Color(image.getRGB(x, y));
                int grayLevel = (c.getBlue()+c.getGreen()+c.getRed())/3;
                if(grayLevel<min) min=grayLevel;
                if(grayLevel>max) max=grayLevel;
            }
        }        
        return new Point(min, max);
    }
    
    private void reArrangeShares(BufferedImage image,int difRange){
        for(int x=0; x<image.getWidth(); x++){
            for(int y=0; y <image.getHeight(); y++){
                Color c =new Color(image.getRGB(x, y));
                double grayLevel = (c.getBlue()+c.getGreen()+c.getRed())/3;
                grayLevel= grayLevel/difRange;
                int range=(int)Math.round(U*255) - (int)Math.round(L*255);
                int newGrayLevel=(int)Math.round(grayLevel*range) + (int)Math.round(L*255);
                image.setRGB(x, y, new Color(newGrayLevel,newGrayLevel,newGrayLevel).getRGB());                
            }
        }
    }
    
    private void reArrangeTarget(BufferedImage image,int difRange){
        for(int x=0; x<image.getWidth(); x++){
            for(int y=0; y <image.getHeight(); y++){
                Color c=new Color(image.getRGB(x, y));
                double grayLevel = (c.getBlue()+c.getGreen()+c.getRed())/3;
                grayLevel= grayLevel/difRange;
                int range=(int)Math.round(L*255) - (int)Math.round(Math.max(0, 2*U-1)*255);
                if(range<0) range=0;
                int newGrayLevel=(int)Math.round(grayLevel*range) + (int)Math.round(Math.max(0, 2*U-1)*255);
                image.setRGB(x, y, new Color(newGrayLevel,newGrayLevel,newGrayLevel).getRGB());                
            }
        }
    }
    
    public void process(String share1Path, String share2Path, String targetPath) throws IOException{

        BufferedImage target = ImageIO.read(getClass().getResourceAsStream("/images/"+targetPath));
        BufferedImage share1 = ImageIO.read(getClass().getResourceAsStream("/images/"+share1Path));
        BufferedImage share2 = ImageIO.read(getClass().getResourceAsStream("/images/"+share2Path));
        
        Point range = this.findRange(share1);
        this.lowBoundS1=range.x;
        this.upperBoundS1=range.y;
        range = this.findRange(share2);
        this.lowBoundS2=range.x;
        this.upperBoundS2=range.y;
        range = this.findRange(target);
        this.lowBoundT=range.x;
        this.upperBoundT=range.y;
        
        this.reArrangeShares(share1, this.upperBoundS1-this.lowBoundS1);
        this.reArrangeShares(share2, this.upperBoundS2-this.lowBoundS2);
        this.reArrangeTarget(target, this.upperBoundT-this.lowBoundT);
        
        range = this.findRange(share1);
        this.lowBoundS1=range.x;
        this.upperBoundS1=range.y;
        range = this.findRange(share2);
        this.lowBoundS2=range.x;
        this.upperBoundS2=range.y;
        range = this.findRange(target);
        this.lowBoundT=range.x;
        this.upperBoundT=range.y;

        File outputS1 = File.createTempFile("pShare1",".png");
        this.outputS1path = outputS1.getAbsolutePath();
        File outputS2 = File.createTempFile("pShare2",".png");
        this.outputS2path = outputS2.getAbsolutePath();
        File outputT = File.createTempFile("pTarget",".png");
        this.outputTpath = outputT.getAbsolutePath();
        
        try {
            ImageIO.write(share1, "png", outputS1);
            ImageIO.write(share2, "png", outputS2);
            ImageIO.write(target, "png", outputT);           

        } catch (IOException ex) {
            Logger.getLogger(PreProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void putMark (String path) throws IOException{
        File input = new File(path);
        BufferedImage image = ImageIO.read(input);
        
        for(int x=0; x<image.getWidth(); x++){
            for(int y=0; y<image.getHeight(); y++){
                if(x==0||y==0||x==image.getWidth()-1||y==image.getHeight()-1){
                    image.setRGB(x, y, new Color(0,0,0).getRGB());
                }
            }
        }        
        ImageIO.write(image, "png", input);        
    }
    
}