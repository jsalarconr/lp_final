package control;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author sebas
 */
public class UncryptAfin extends CryptoSystem{
    
    public UncryptAfin(){
        super();
    }
    
    public void crypt(String plaintext, String key_a, String key_b) {
        plaintext = plaintext.toUpperCase().replaceAll("\\s+","");
        int a = uncryptctrl.mod(Integer.parseInt(key_a), 26);
        int b = uncryptctrl.mod(Integer.parseInt(key_b), 26);
        ArrayList cipher_codes = new ArrayList();
        ArrayList<Integer> codes = uncryptctrl.convertStringToCode(plaintext);
        for (Integer code : codes) {
            int number = uncryptctrl.mod(code*a + b, 26);
            cipher_codes.add(number);
        }
        
        this.setCiphertext(uncryptctrl.convertCodeToString(cipher_codes));
    }
    
    public void decrypt(String ciphertext, String key_a, String key_b){
        ciphertext = ciphertext.toUpperCase().replaceAll("\\s+","");
        int a = uncryptctrl.mod(Integer.parseInt(key_a), 26);
        int b = uncryptctrl.mod(Integer.parseInt(key_b), 26);
        ArrayList<Integer> cipher_codes = uncryptctrl.convertStringToCode(ciphertext);
        ArrayList codes = new ArrayList();
        
        for (Integer code : cipher_codes) {
            int number = uncryptctrl.mod(uncryptctrl.INVERSE.get(a)*(code - b), 26);
            codes.add(number);
        }
        
        this.setPlaintext(uncryptctrl.convertCodeToString(codes));
    }
    
    public int generateRandomAKey(){
        Random random = new Random();
        int randomNum = random.nextInt((26 - 0) + 1) + 0;
        if(uncryptctrl.gcd(randomNum, 26) == 1){
            return randomNum;
        }
        return generateRandomAKey();
    }
    
}
