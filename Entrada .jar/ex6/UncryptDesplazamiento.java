package control;

import java.util.ArrayList;

/**
 *
 * @author sebas
 */
public class UncryptDesplazamiento extends CryptoSystem{
   
    public UncryptDesplazamiento(){
        super();
    }   
    
    public void crypt(String plaintext, String key_str){
        plaintext = plaintext.toUpperCase().replaceAll("\\s+","");
        int key = uncryptctrl.mod(Integer.parseInt(key_str), 26);
        ArrayList cipher_codes = new ArrayList();
        ArrayList<Integer> codes = uncryptctrl.convertStringToCode(plaintext);
        for (Integer code : codes) {
            int number = uncryptctrl.mod(code + key, 26);
            cipher_codes.add(number);
        }
        
        this.setCiphertext(uncryptctrl.convertCodeToString(cipher_codes));
    }
    
    public void decrypt(String ciphertext, String key_str){
        ciphertext = ciphertext.toUpperCase().replaceAll("\\s+","");
        int key = uncryptctrl.mod(new Double(key_str).intValue(), 26);
        ArrayList plaint_codes = new ArrayList();
        ArrayList<Integer> codes = uncryptctrl.convertStringToCode(ciphertext);
        for (Integer code : codes) {
            int number = uncryptctrl.mod(code - key, 26);
            plaint_codes.add(number);
        }
        
        this.setPlaintext(uncryptctrl.convertCodeToString(plaint_codes));
    }
}
