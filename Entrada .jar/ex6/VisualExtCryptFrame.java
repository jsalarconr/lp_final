/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.EVC;
import control.PreProcess;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 *
 * @author sebas
 */
public class VisualExtCryptFrame extends javax.swing.JFrame {

    public VisualExtCryptFrame() {
        preprocessCtrl = new PreProcess();
        evcCtrl = new EVC();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        preprocessButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        share1Label = new javax.swing.JLabel();
        share2Label = new javax.swing.JLabel();
        targetLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        encrypt = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("DejaVu Sans", 1, 22)); // NOI18N
        jLabel1.setText("Criptografía Visual Extendida");

        preprocessButton.setText("Procesar");
        preprocessButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preprocessButtonActionPerformed(evt);
            }
        });

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        share1Label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/simpsons1.jpg"))); // NOI18N

        share2Label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/simpsons2.jpg"))); // NOI18N

        targetLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/simpsons3.jpg"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel2.setText("Sombra 1");

        jLabel3.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel3.setText("Sombra 2");

        jLabel4.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel4.setText("Objetivo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(share1Label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(share2Label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(targetLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(162, 162, 162)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(349, 349, 349)
                .addComponent(jLabel4)
                .addGap(172, 172, 172))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(targetLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(share2Label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(share1Label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        encrypt.setText("Encriptar");
        encrypt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encryptActionPerformed(evt);
            }
        });

        backButton.setText("Volver");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        jMenu1.setText("Archivo");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Acerca de");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(preprocessButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(encrypt, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(backButton)
                            .addComponent(jLabel1))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addGap(3, 3, 3)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(preprocessButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(encrypt)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(backButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static BufferedImage resize(BufferedImage image, int width, int height) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = (Graphics2D) bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();
        return bi;
    }
    
    private void preprocessButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preprocessButtonActionPerformed
        // TODO add your handling code here:
        try {
            preprocessCtrl.process(path1, path2, path3);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        ImageIcon icon1 = new ImageIcon(preprocessCtrl.getOutputS1path());
        share1Label.setIcon(icon1);
        icon1 = new ImageIcon(preprocessCtrl.getOutputS2path());
        share2Label.setIcon(icon1);
        icon1 = new ImageIcon(preprocessCtrl.getOutputTpath());
        targetLabel.setIcon(icon1);
        this.isPreProcces=true;
    }//GEN-LAST:event_preprocessButtonActionPerformed

    private void encryptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encryptActionPerformed
        
        File inputS1;
        File inputS2;
        File inputTarget;
        if(this.isPreProcces){
            inputS1 = new File(preprocessCtrl.getOutputS1path());
            inputS2 = new File(preprocessCtrl.getOutputS2path());
            inputTarget = new File(preprocessCtrl.getOutputTpath());
        }else{
            inputS1 = new File(getClass().getResource("/images/"+path1).getFile());
            inputS2 = new File(getClass().getResource("/images/"+path2).getFile());
            inputTarget = new File(getClass().getResource("/images/"+path3).getFile());
        }

        BufferedImage share1=null;
        BufferedImage share2=null;
        BufferedImage target=null;
        try {
            share1 = ImageIO.read(inputS1);
            share2 = ImageIO.read(inputS2);
            target = ImageIO.read(inputTarget);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            evcCtrl.encrypt(share1, share2, target);
        } catch (IOException ex) {
            Logger.getLogger(VisualExtCryptFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        File inputFile1 = new File(evcCtrl.getOutputFile3Path());
        File inputFile2 = new File(evcCtrl.getOutputFile4Path());
        File inputFile3 = new File(evcCtrl.getOutputFile5Path());
        File inputFile4 = new File(evcCtrl.getOutputFile1Path());
        File inputFile5 = new File(evcCtrl.getOutputFile2Path());

        BufferedImage image1 = null;
        BufferedImage image2 = null;
        BufferedImage image3 = null;
        BufferedImage image4 = null;
        BufferedImage image5 = null;
        try {
            image1 = ImageIO.read(inputFile1);
            image2 = ImageIO.read(inputFile2);
            image3 = ImageIO.read(inputFile3);
            image4 = ImageIO.read(inputFile4);
            image5 = ImageIO.read(inputFile5);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedImage resizedImage4 = resize(image4,image4.getWidth()/2,image4.getHeight()/2);
        BufferedImage resizedImage5 = resize(image5,image5.getWidth()/2,image5.getHeight()/2);
       
        ImageIcon icon4 = new ImageIcon(resizedImage4);
        ImageIcon icon5 = new ImageIcon(resizedImage5);
        
        VisualExtCryptFrameHelper vecFrameHelper = new VisualExtCryptFrameHelper(icon4, icon5, evcCtrl);
        vecFrameHelper.setVisible(true);
        //vecFrameHelper.setExtendedState(vecFrameHelper.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        
    }//GEN-LAST:event_encryptActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_backButtonActionPerformed
    
    String path1 = "simpsons1.jpg";
    String path2 = "simpsons2.jpg";
    String path3 = "simpsons3.jpg";
    //ImageIcon icon1 = new javax.swing.ImageIcon(getClass().getResource("/images/"+path1));
    //ImageIcon icon2 = new javax.swing.ImageIcon(getClass().getResource("/images/"+path2));
    //ImageIcon icon3 = new javax.swing.ImageIcon(getClass().getResource("/images/"+path3));
    
    PreProcess preprocessCtrl;
    Boolean isPreProcces=false;
    
    EVC evcCtrl;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JButton encrypt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton preprocessButton;
    private javax.swing.JLabel share1Label;
    private javax.swing.JLabel share2Label;
    private javax.swing.JLabel targetLabel;
    // End of variables declaration//GEN-END:variables
}
