package control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author sebas
 */
public class UncryptVigenere extends CryptoSystem {
    
    public UncryptVigenere(){
        super();
    }
    
    public void crypt(String plaintext, String key){
        ArrayList cipher_codes = new ArrayList();
        plaintext = plaintext.toUpperCase().replaceAll("\\s+","");
        key = key.toUpperCase().replaceAll("\\s+","");
        ArrayList<Integer> codes = uncryptctrl.convertStringToCode(plaintext);
        ArrayList<Integer> key_codes = uncryptctrl.convertStringToCode(key);
        int keylength = key_codes.size();
        for(int i = 0; i < codes.size(); i++ ){    
            cipher_codes.add(uncryptctrl.mod(codes.get(i) +  key_codes.get(uncryptctrl.mod(i, keylength)), 26));
        }
        this.setCiphertext(uncryptctrl.convertCodeToString(cipher_codes));
    }
    
    public void decrypt(String ciphertext, String key){
        ArrayList codes = new ArrayList();
        ciphertext = ciphertext.toUpperCase().replaceAll("\\s+","");
        key = key.toUpperCase().replaceAll("\\s+","");
        ArrayList<Integer> cipher_codes = uncryptctrl.convertStringToCode(ciphertext);
        ArrayList<Integer> key_codes = uncryptctrl.convertStringToCode(key);
        int keylength = key_codes.size();
        for(int i = 0; i < cipher_codes.size(); i++ ){    
            codes.add(uncryptctrl.mod(cipher_codes.get(i) -  key_codes.get(uncryptctrl.mod(i, keylength)), 26));
        }
        
        this.setPlaintext(uncryptctrl.convertCodeToString(codes));
    }
    
    public String generateRandomKey(){
        Random random = new Random();
        int keylength = random.nextInt((10 - 3) + 1) + 3;
        ArrayList<Integer> randomKey = new ArrayList();
        for(int i =0;i<keylength;i++){
            randomKey.add(random.nextInt((25 - 0) + 1) + 0);
        }
        return uncryptctrl.convertCodeToString(randomKey);
    }
    
    public ArrayList<String> divide(String str, int numGroups) {
        ArrayList groups = new ArrayList();
        ArrayList<String> result = new ArrayList();
        for (int i = 0; i < numGroups; ++i) {
            groups.add(new ArrayList());
        }
		
        for (int i = 0; i < str.length(); ++i) {
            ArrayList thisArrayList = (ArrayList) groups.get( i % numGroups);
            thisArrayList.add(Character.toString(str.charAt(i)));            
        }
        
        for (int i = 0; i < numGroups; ++i) {
            result.add(String.join("", (ArrayList) groups.get( i % numGroups)));
        }
        return result;
    }
    
    public HashMap getFrequencyTable(String str) {
        HashMap<String,Integer> charCount = new HashMap<>();
        HashMap<String,Double> charFrequency = new HashMap<>();
        for (int i = 0; i < 26; ++i) {
            charCount.put(Character.toString((char) ((char) i+65)) , 0);
        }

        for (int i = 0; i < str.length(); ++i) {
            String key = Character.toString(str.charAt(i));
            Integer value = charCount.get(key);
            value++;
            charCount.put(key,value);
        }
        
        
        // frequency of each char in the string
        for (int i = 0; i < 26; ++i) {
            String key = Character.toString((char) ((char) i+65));
            Integer value = charCount.get(key);
            Double freq = (double) value/str.length();
            charFrequency.put(key, freq);
        }

        return charFrequency;
    }
    public double indexOfCoincidence(HashMap<String, Double> frequencyTable) {
        Double idx = 0.0;
        for (int i = 0; i < 26; ++i) {
              String key = Character.toString((char) ((char) i+65));
              Double value = frequencyTable.get(key);
              Double standarFreqValue = Uncrypt.STANDARDFREQUENCY.get(key);
              idx += value * standarFreqValue;
        }
        return idx;
    }
    
    public double[] bestCaeserShift(String ciphertxt) {
        UncryptDesplazamiento despCtrl = new UncryptDesplazamiento();
        double bestShiftAmount = 0;
        double bestDifference = Integer.MAX_VALUE;
        String pt;
        for (int shiftAmount = 0; shiftAmount < 26; ++shiftAmount) {
            despCtrl.decrypt(ciphertxt, String.valueOf(shiftAmount));
            pt = despCtrl.getPlaintext();
            double index = indexOfCoincidence(getFrequencyTable(pt));
            double difference = Math.abs(index - 0.065); // 0.065 is the index for natural English
            if (difference < bestDifference) {
                bestDifference = difference;
                bestShiftAmount = shiftAmount;
            }
        }
        
        return new double[]{bestShiftAmount, bestDifference};
    }
    
    
    public ArrayList<VigenereCrackHelper> crack(String ciphertext, int maxKeyLength) {
        ArrayList<VigenereCrackHelper> overall = new ArrayList();
        ArrayList<Double> key = new ArrayList();
        ArrayList<Integer> keyInt = new ArrayList();
        ArrayList groups = new ArrayList();
        ArrayList<String> plaintexts = new ArrayList();
        ArrayList pt = new ArrayList();
        double totalDifference=0;
        double difference=0;
        double shiftAmount = 0;
        double[] info = {0,0};
        String resultPlainText = "";
        String keyString ="";
        UncryptDesplazamiento despCtrl = new UncryptDesplazamiento();
        for(int keyLen = 1; keyLen <= maxKeyLength; ++keyLen){
            key.clear();
            keyInt.clear();
            groups = divide(ciphertext, keyLen);
            //System.out.println(""+groups.toString());
            totalDifference = 0;
            plaintexts.clear();
            for ( int groupIndex = 0; groupIndex < groups.size(); ++groupIndex) {
                String group = (String) groups.get(groupIndex);
                info = bestCaeserShift(group);
                shiftAmount = info[0];
                difference = info[1];
                totalDifference += difference;
		despCtrl.decrypt(group, String.valueOf(shiftAmount));
                plaintexts.add(despCtrl.getPlaintext());
		key.add(shiftAmount);
            }
            pt.clear();
            for (int i = 0; i < ciphertext.length(); ++i) {
                pt.add(plaintexts.get(i%keyLen).charAt(i/keyLen));
            }
            resultPlainText = "";
            for (Object s : pt){
                resultPlainText += s;
            }
            
            for(Double obj : key){
                keyInt.add(obj.intValue());
            }
            keyString = uncryptctrl.convertCodeToString(keyInt) ;
            overall.add(new VigenereCrackHelper(resultPlainText, totalDifference, keyString));
            //overall.add(new Object[]{});
        }
        
        return overall;
    }
    
}
