package control;

import java.util.ArrayList;

/**
 *
 * @author sebas
 */
public class VigenereCrackHelper {
    public double difference;
    public String plaintext;
    public String key;

    VigenereCrackHelper(String resultPlainText, double totalDifference, String keyString) {
        this.difference = totalDifference;
        this.key = keyString;
        this.plaintext = resultPlainText;
    }
}
