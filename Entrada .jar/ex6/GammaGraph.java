/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author sebas
 */
public class GammaGraph extends JPanel{
    
    Font font = new Font("MS Sans Serif", Font.PLAIN, 9);
    private double xMin, xMax, yMin, yMax, initx, inity;
    Line2D line = new Line2D.Double();
    Rectangle2D rect = new Rectangle2D.Double();
    byte table[][];
    public boolean drawGraph = false, isDrawGraph = false, graph1 = true;
    Point initialPoint = new Point(0, 0);
    Point point = new Point();
    Point pointAux = new Point();
    Point point1 = new Point();
    Point point2 = new Point();
    Point point1a = new Point();
    Point point2a = new Point();
    public GammaGraph(double xMin, double xMax, double yMin, double yMax, byte table[][], Point initPoint, boolean type) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.initialPoint = initPoint;
        //System.out.println(this.yMin);
        //System.out.println(this.xMin);
        //System.out.println(this.xMax);
        System.out.println(initialPoint);
        
        this.setPreferredSize(new Dimension(20 * (int) Math.abs(xMin - xMax),
                                        15 * (int) Math.abs(yMin - yMax)));
        this.drawGraph = true;
        this.table = table;
        if(type == false){
            this.graph1 = false;
        }
        this.repaint();
    }
    
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.CYAN);
        line.setLine(CoorX(this.xMin), CoorY(0), CoorX(this.xMax), CoorY(0));
        g2.draw(line);
        line.setLine(CoorX(0), CoorY(this.yMin), CoorX(0), CoorY(this.yMax));
        g2.draw(line);
        g2.setColor(Color.BLACK);
        for (int i = 0; i < this.xMax; i++) {
            for (int j = 0; j < this.yMax; j++) {
                rect.setFrame(CoorX(i), CoorY(j) - 1, 2, 2);
                g2.fill(rect);
           }
        }
        if (drawGraph) {
            point.setLocation(initialPoint);
            if (graph1) {
                drawGraph1(point, g2);
            }else {
                drawGraph2(point, g2);
            }
            g2.setColor(Color.RED);
            rect.setFrame(CoorX(initialPoint.x) - 3, CoorY(initialPoint.y) - 3, 6, 6);
            g2.fill(rect);
            //canvastext.repaint();
            this.isDrawGraph = true;
        }
    }
    
    private void drawGraph1(Point point, Graphics2D g2) {
        pointAux.setLocation(point);
        point.setLocation(initialPoint);
        drawBranch(point, g2);
        point.setLocation(point.getX() + 1, point.getY());
        pointAux.setLocation(point);
        for (int i = 0; i < this.xMax; i++) {
          for (int j = 0; point.getY() < this.yMax && point.getX() < this.xMax;
               j++) {
            drawBranch(point, g2);
            point.setLocation(point.getX() + 1, point.getY() + j + 1);
          }
          point.setLocation(pointAux.getX() + 2, pointAux.getY() + 1);
          pointAux.setLocation(point);
        }
      }
    private void drawGraph2(Point point, Graphics2D g2) {
        pointAux.setLocation(point);
        for (int i = 0; pointAux.getY() < this.yMax && pointAux.getX() < this.xMax;i++) {
            drawBranch(pointAux, g2);
            pointAux.setLocation(pointAux.x + 1, pointAux.y + i);
        }
    }
    
    private void drawBranch(Point point, Graphics2D g2) {
        point1.setLocation(point);
        point2.setLocation(point.getX() + 1, point.getY());
        for (int i = 0;point2.getY() < this.yMax && point2.getX() < this.xMax; i++) {
            g2.setColor(Color.BLUE);
            line.setLine(CoorX(point1.getX()), CoorY(point1.getY()),CoorX(point2.getX()), CoorY(point2.getY()));
            //if (point.equals(initialPoint)) {
            g2.draw(line);
            //}
            if (!this.isDrawGraph) {
                //canvastext.modificateTable(point1, point2);
            }

            if (!graph1) {
                drawSubBranch(point2, i, g2);
            }
            point1.setLocation(point2);
            point2.setLocation(point2.getX() + 1, point2.getY() + i + 1);
        }
    }
    
    private void drawSubBranch(Point point, int lenght, Graphics2D g2) {
        point1a.setLocation(point);
        point2a.setLocation(point.getX() + 1, point.getY());
        for (int i = 0; i <= lenght && point2a.getY() < this.yMax && point2a.getX() < this.xMax; i++) {
            g2.setColor(Color.MAGENTA);
            line.setLine(CoorX(point1a.getX()), CoorY(point1a.getY()), CoorX(point2a.getX()), CoorY(point2a.getY()));
            g2.draw(line);
            if (!this.isDrawGraph) {
                //canvastext.modificateTable(point1a, point2a);
            }
            point1a.setLocation(point2a);
            point2a.setLocation(point2a.getX() + 1, point2a.getY() + i + 1);
        }
    }
    
    private double CoorX(double x) {
        return (this.getWidth() * (x - this.xMin)) / (this.xMax - this.xMin);
    }

    private double CoorY(double y) {
        return ( -this.getHeight() * (y - this.yMax)) / (this.yMax - this.yMin);
    }
    
    public void setInitialPoint(Point point) {
        this.initialPoint = point;
    }
    
    public void setLimits(double xMin, double xMax, double yMin, double yMax) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.setPreferredSize(new Dimension(20 * (int) Math.abs(xMin - xMax),
                                        15 * (int) Math.abs(yMin - yMax)));
    }
}