package control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


/**
 *
 * @author sebas
 */
public class UncryptSustitucion extends CryptoSystem{
    
    public UncryptSustitucion(){
        super();
    }
    
    public void crypt(String plaintext, String key_str){
        plaintext = plaintext.toUpperCase().replaceAll("\\s+","");
        String[] result = key_str.split(",");
        String ciph_text = "";
        for(int i =0;i<result.length; i++){
            result[i]=result[i].toUpperCase();
        }
        for(int i=0;i<plaintext.length();i++){
            ciph_text += result[plaintext.charAt(i)-65];
        }
        this.setCiphertext(ciph_text);
    }
    
    public void decrypt(String ciphertext, String key_str){
        ciphertext = ciphertext.toUpperCase().replaceAll("\\s+","");
        String[] result = key_str.split(",");
        ArrayList<Integer> plaint_codes = new ArrayList();
        ArrayList<String> cipher_codes = new ArrayList();
        
        for (String result1 : result) {
            cipher_codes.add(result1.toUpperCase());	
        }
        
        for(int i=0;i<ciphertext.length();i++){
            plaint_codes.add(cipher_codes.indexOf(Character.toString(ciphertext.charAt(i))));
        }

        this.setPlaintext(uncryptctrl.convertCodeToString(plaint_codes));
    }
    
    public static boolean compareArrays(String[] arr1, String[] arr2) {
        HashSet<String> set1 = new HashSet<String>(Arrays.asList(arr1));
        HashSet<String> set2 = new HashSet<String>(Arrays.asList(arr2));
        return set1.equals(set2);
    }
    
    public List generateRandomKey(){
        List<String> alphabetList = Arrays.asList(uncryptctrl.ALPHABET);
        java.util.Collections.shuffle(alphabetList);
        return alphabetList;
    }
    
    public boolean checkKey (String string_key){
        String[] result = string_key.split(",");
        if(result.length!=26){
            return false;
        }
        
        for(int i =0;i<result.length; i++){
            result[i]=result[i].toUpperCase();
        }
        
        if(!compareArrays(result, uncryptctrl.ALPHABET)){
            return false;
        }
        return true;
    }
}
