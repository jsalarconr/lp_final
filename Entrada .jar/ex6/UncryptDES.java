package control;

import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author sebas
 */
public class UncryptDES extends CryptoSystem{
    
    Cipher cipher;
    private String key;
    
    public String generateKey() throws Exception{
        SecretKey key = KeyGenerator.getInstance("DESede").generateKey();
        String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
        return encodedKey;
    }

    public String checkFields(String message, String key, boolean isEncrypt) {
        return "success";
    }

    public void crypt(String pt,String key) throws Exception{
        this.key=key;
        this.setPlaintext(pt);
        this.setCiphertext("");
        byte[] decodedKey = Base64.getDecoder().decode(key);
        SecretKey key2 = new SecretKeySpec(decodedKey, 0, decodedKey.length, "DESede"); 
        cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.ENCRYPT_MODE, key2);
        // Encode the string into bytes using utf-8
        byte[] utf8 = this.getPlaintext().getBytes("UTF8");
        // Encrypt
        byte[] enc = cipher.doFinal(utf8);
        setCiphertext(new sun.misc.BASE64Encoder().encode(enc));
    }

    public void decrypt(String ct,String key) throws Exception{
        this.key=key;
        this.setCiphertext(ct);
        this.setPlaintext("");
        byte[] decodedKey = Base64.getDecoder().decode(key);
        SecretKey key2 = new SecretKeySpec(decodedKey, 0, decodedKey.length, "DESede");
        cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.DECRYPT_MODE, key2);
         // Decode base64 to get bytes
        byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(getCiphertext());
        byte[] utf8 = cipher.doFinal(dec);
        // Decode using utf-8
        setPlaintext(new String(utf8, "UTF8"));
    }    
}
