/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;

/**
 *
 * @author sebas
 */
public class ExtendedEuclideanAlgorithm { // mcd(n,p)=xn+yp;    n^(-1)=x mod p si MCD(n,p)=1
    
    private int mcd;
    private int x;
    private int y;
    private ArrayList<Integer> r;
    private ArrayList<Integer> s;
    private ArrayList<Integer> t;
    
    public int getMcd() {
        return mcd;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public ExtendedEuclideanAlgorithm(int n, int p){
        r=new ArrayList<>();
        s=new ArrayList<>();
        t=new ArrayList<>();
        r.add(n);
        r.add(p);
        s.add(1);
        s.add(0);
        t.add(0);
        t.add(1);
        execute();
    }
    
    private void execute(){
        int i=1;
        while (r.get(i)!=0){
            int q=r.get(i-1)/r.get(i);
            r.add(r.get(i-1)%r.get(i));
            s.add(s.get(i-1)-q*s.get(i));
            t.add(t.get(i-1)-q*t.get(i));
            i++;
        }
        this.mcd=this.r.get(i-1);
        this.x=this.s.get(i-1);
        this.y=this.t.get(i-1);
    }
    
}
