/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Point;
import javax.swing.JFrame;

/**
 *
 * @author sebas
 */
public class GammaFrame extends javax.swing.JFrame {

    /**
     * Creates new form GammaFrame
     */
    
    private int maxx, maxy, minx, miny;
    private Point initial_point;
    private byte[][] table;
    private boolean type;
    public GammaFrame() {
        initComponents();
    }

    GammaFrame(Point initial_point, int maxx, int maxy, int minx, int miny, byte[][] table, boolean type) {
        this.initial_point = initial_point;
        this.maxx = maxx;
        this.minx = minx;
        this.miny = miny;
        this.maxy = maxy;
        this.table = table;
        this.type = type;
        initComponents();
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        gammaGraph1 = new GammaGraph(this.minx, this.maxx, this.miny, this.maxy, this.table, this.initial_point, this.type);
        jPanel5 = new javax.swing.JPanel();
        spin_maxy = new javax.swing.JSpinner();
        spin_miny = new javax.swing.JSpinner();
        spin_maxx = new javax.swing.JSpinner();
        spin_minx = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        init_x = new javax.swing.JSpinner();
        init_y = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gamma Pentagonal");
        setPreferredSize(new java.awt.Dimension(1366, 768));

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gamma Graph", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        gammaGraph1.setBorder(null);

        javax.swing.GroupLayout gammaGraph1Layout = new javax.swing.GroupLayout(gammaGraph1);
        gammaGraph1.setLayout(gammaGraph1Layout);
        gammaGraph1Layout.setHorizontalGroup(
            gammaGraph1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 791, Short.MAX_VALUE)
        );
        gammaGraph1Layout.setVerticalGroup(
            gammaGraph1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 535, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(gammaGraph1);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Coordinates", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));

        spin_maxy.setValue(20);
        spin_maxy.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spin_maxyStateChanged(evt);
            }
        });

        spin_miny.setValue(-10);
        spin_miny.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spin_minyStateChanged(evt);
            }
        });

        spin_maxx.setValue(10);
        spin_maxx.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spin_maxxStateChanged(evt);
            }
        });

        spin_minx.setValue(-10);
        spin_minx.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spin_minxStateChanged(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("+y");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("- y");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("+x");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("- x");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(spin_minx, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(spin_maxx, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(99, 99, 99))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(spin_miny)
                            .addComponent(spin_maxy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(111, 111, 111))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spin_maxx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spin_minx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(88, 88, 88))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(spin_maxy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(spin_miny, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Initial Point", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));

        init_x.setValue(-8);
        init_x.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                init_xStateChanged(evt);
            }
        });

        init_y.setValue(-6);
        init_y.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                init_yStateChanged(evt);
            }
        });

        jLabel7.setText("X");

        jLabel8.setText("Y");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(init_x, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(init_y, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(init_x, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(init_y, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addContainerGap(92, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(112, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void spin_maxyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spin_maxyStateChanged
        gammaGraph1.setLimits( Integer.parseInt(spin_minx.getValue().toString()), Integer.parseInt(spin_maxx.getValue().toString()),Integer.parseInt(spin_miny.getValue().toString()) , Integer.parseInt(spin_maxy.getValue().toString()));
        gammaGraph1.repaint();
    }//GEN-LAST:event_spin_maxyStateChanged

    private void spin_minyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spin_minyStateChanged
        gammaGraph1.setLimits( Integer.parseInt(spin_minx.getValue().toString()), Integer.parseInt(spin_maxx.getValue().toString()),Integer.parseInt(spin_miny.getValue().toString()) , Integer.parseInt(spin_maxy.getValue().toString()));
        gammaGraph1.repaint();
    }//GEN-LAST:event_spin_minyStateChanged

    private void spin_maxxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spin_maxxStateChanged
        gammaGraph1.setLimits( Integer.parseInt(spin_minx.getValue().toString()), Integer.parseInt(spin_maxx.getValue().toString()),Integer.parseInt(spin_miny.getValue().toString()) , Integer.parseInt(spin_maxy.getValue().toString()));
        gammaGraph1.repaint();
    }//GEN-LAST:event_spin_maxxStateChanged

    private void spin_minxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spin_minxStateChanged
        gammaGraph1.setLimits( Integer.parseInt(spin_minx.getValue().toString()), Integer.parseInt(spin_maxx.getValue().toString()),Integer.parseInt(spin_miny.getValue().toString()) , Integer.parseInt(spin_maxy.getValue().toString()));
        gammaGraph1.repaint();
    }//GEN-LAST:event_spin_minxStateChanged

    private void init_xStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_init_xStateChanged
        int Xo = Integer.parseInt(this.init_x.getValue().toString());
        int Yo = Integer.parseInt(this.init_y.getValue().toString());
        gammaGraph1.setInitialPoint(new Point(Xo, Yo));
        gammaGraph1.repaint();
    }//GEN-LAST:event_init_xStateChanged

    private void init_yStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_init_yStateChanged
        int Xo = Integer.parseInt(this.init_x.getValue().toString());
        int Yo = Integer.parseInt(this.init_y.getValue().toString());
        gammaGraph1.setInitialPoint(new Point(Xo, Yo));
        gammaGraph1.repaint();
    }//GEN-LAST:event_init_yStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private view.GammaGraph gammaGraph1;
    private javax.swing.JSpinner init_x;
    private javax.swing.JSpinner init_y;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner spin_maxx;
    private javax.swing.JSpinner spin_maxy;
    private javax.swing.JSpinner spin_minx;
    private javax.swing.JSpinner spin_miny;
    // End of variables declaration//GEN-END:variables
}