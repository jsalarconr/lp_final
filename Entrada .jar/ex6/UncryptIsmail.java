
package control;

import Jama.Matrix;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sebas
 */
public class UncryptIsmail {
    private String key;
    private boolean isIsmail;
    private double[][] keyArray;
    private Matrix keyMatrix;
    private int length;
    private int [][] plainImage; 
    private int [][] cipherImage;
    private final List<Integer> invertZ_256;
    private Matrix IV;
    
    public UncryptIsmail(){
        this.invertZ_256= new ArrayList<>();
        ExtendedEuclideanAlgorithm ea;
        for(int i=1; i<256; i++){
            ea=new ExtendedEuclideanAlgorithm(i,256);
            if(ea.getMcd()==1){
                this.invertZ_256.add(i);
            }
        }
    }
    
    public void reCalculateKey(){
        double[][] a;
        for(int i=0; i<this.keyArray.length; i++){
            a=this.IV.times(this.keyMatrix).getArray();
            for(int j=0; j<this.keyArray[0].length; j++){
                this.keyArray[i][j]=a[0][j]%256;
                if(this.keyArray[i][j]<0) this.keyArray[i][j]+=256;
            }
            this.keyMatrix=new Matrix(this.keyArray);
        }
    }
    
    public String generateKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String checkFields(int[][] image, String key, String iv, boolean type){
        this.key=key;
        this.isIsmail=type;
        String result = this.readKey(key);
        if(!result.equals("success")) return result;
        result=readIV(iv);
        if(!result.equals("success")) return result;
        if(image.length*image[0].length%this.length!=0) return "El número de pixeles de la imagen debe ser divisible por el tamaño de la matriz";
        this.keyMatrix=new Matrix(this.keyArray);
        result = this.checkKey();
        if(!result.equals("success")) return result;
        return "success";
    }
    
    public String readKey (String key){        
        String[] auxKey = key.split(",");
        double sq = java.lang.Math.sqrt(auxKey.length);
        if ((int)sq - sq != 0) {
            return "Por favor ingrese una clave válida. La matriz debe ser invertible";
        }
        this.length = (int)sq;       
        this.keyArray = new double[length][length];        
        int auxCount = 0;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                this.keyArray[i][j] = Double.valueOf(auxKey[auxCount]);
                auxCount ++;
            }
        }
        return "success";
    }
    
    public String readIV (String iv){
        if(!this.isIsmail) return "success";
        String[] auxIV = iv.split(",");
        if(auxIV.length!=this.length) return "Por favor ingrese un Vector Inicial (IV) válido";
        int auxCount=0;
        double [][] ivArray=new double[1][auxIV.length];
        for (int j = 0; j < length; j++) {
            ivArray[0][j] = Double.valueOf(auxIV[auxCount]);
            auxCount ++;
        }        
        this.IV=new Matrix(ivArray);
        return "success";
    }
    
    public String checkKey() {
        int det =((int)Math.round(this.keyMatrix.det()))%256;
        if (det<0) det+=256;
        for(Integer i:this.invertZ_256){
            if(det==i) return "success";
        }
        return "Por favor ingrese una clave válida. La matriz debe ser invertible";
    }
    
    public void printMatrix(Matrix m){
        double [][] array = m.getArray();
        System.out.println();
        for(int i=0; i<array.length;i++){
            for(int j=0; j<array[0].length; j++){
                System.out.print(array[i][j]+",");
            }
            System.out.print("\n");
        }
    }

    public void encrypt(String key, int [][] image) {
        this.plainImage=image;  
        this.cipherImage=new int [image.length][image[0].length];
        Matrix v;
        double [][] vector = new double [1][this.length];
        int x=0,y=0,index=0;
        for(int i=0; i<image.length; i++){
            for(int j=0; j<image[0].length; j++){
                vector[0][index]=(double)image[i][j];
                if(index==this.length-1){
                    v=new Matrix(vector);
                    vector=v.times(this.keyMatrix).getArray();
                    for(int h=0; h<this.length; h++){
                        int result;
                        result = (int)Math.round(vector[0][h])%256;
                        if(result<0) result+=256;
                        if(y==this.cipherImage[0].length){
                            y=0;
                            x++;
                        }
                        this.cipherImage[x][y]=result;
                        y++;
                    }
                    index=0;
                    if(this.isIsmail) this.reCalculateKey();
                }else index++;
            }
        }                
    }

    public void decrypt(String key, int[][] image) {
        this.cipherImage=image;  
        this.plainImage=new int [image.length][image[0].length];
        Matrix v;
        double [][] vector = new double [1][this.length];
        int x=0, y=0, index=0;
        
        for(int i=0; i<image.length; i++){
            for(int j=0; j<image[0].length; j++){
                vector[0][index]=(double)image[i][j];
                if(index==this.length-1){
                    Matrix inverseKey = this.keyMatrix.inverse().times(this.keyMatrix.det());
                    int det = ((int)Math.round(this.keyMatrix.det()))%256;
                    if(det<0) det+=256;
                    ExtendedEuclideanAlgorithm eea = new ExtendedEuclideanAlgorithm(det,256);
                    inverseKey = inverseKey.times(eea.getX());

                    double [][] inverse = inverseKey.getArray();
                    for(int m=0; m<this.length;m++){
                        for(int n=0; n<this.length; n++){
                            inverse[m][n]=(double)Math.round(inverse[m][n])%256;
                            if(inverse[m][n]<0) inverse[m][n]+=256;
                        }
                    }
                    inverseKey = new Matrix(inverse);
                    v=new Matrix(vector);
                    vector=v.times(inverseKey).getArray();
                    
                    for(int h=0; h<this.length; h++){
                        int result;
                        result = (int)Math.round(vector[0][h])%256;
                        if(result<0) result+=256;
                        if(y==this.plainImage[0].length){
                            y=0;
                            x++;
                        }
                        this.plainImage[x][y]=result;
                        y++;
                    }
                    index=0;
                    if(this.isIsmail) this.reCalculateKey();
                }else index++;
            }
        }
    }

    public int[][] getCipherImage() {
        return this.cipherImage;
    }

    public int[][] getPlainImage() {
        return this.plainImage;
    }

    public String getKey() {
        return (this.key);
    }
}
