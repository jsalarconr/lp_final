/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.GammaPentagonal;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author sebas
 */
public class CoordGraph extends JPanel{
    Font font = new Font("MS Sans Serif", Font.PLAIN, 9);
    private double xMin, xMax, yMin, yMax;
    Line2D line = new Line2D.Double();
    Rectangle2D rect = new Rectangle2D.Double();
    byte table[][];
    Point point = new Point();
    Point initialPoint = new Point(0, 0);
    public CoordGraph(double xMin, double xMax, double yMin, double yMax, byte table[][]) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.setPreferredSize(new Dimension(40 * (int) Math.abs(xMin - xMax),
                                        20 * (int) Math.abs(yMin - yMax)));
        this.table = table;
    }
    
    @Override
    public void paint(Graphics g) {
        g.setFont(font);
        g.clearRect(0, 0, getWidth(), getHeight());
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.CYAN);
        line.setLine(CoorX(this.xMin), CoorY(0), CoorX(this.xMax), CoorY(0));
        g2.draw(line);
        line.setLine(CoorX(0), CoorY(this.yMin), CoorX(0), CoorY(this.yMax));
        g2.draw(line);
        g2.setColor(Color.BLACK);

        for (int i = 0; i < this.xMax; i++) {
          for (int j = 0; j < this.yMax; j++) {
            rect.setFrame(CoorX(i), CoorY(j) - 1, 2, 2);
            g2.fill(rect);
          }
        }
        paintTable(this.table, g2);
  }
    
    
    private double CoorX(double x) {
        return (this.getWidth() * (x - this.xMin)) / (this.xMax - this.xMin);
    }

    private double CoorY(double y) {
        return ( -this.getHeight() * (y - this.yMax)) / (this.yMax - this.yMin);
    }
    
    
    private void paintTable(byte[][] table, Graphics2D g2) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[0].length; j++) {
                g2.drawString(Character.toString( (char) table[i][j]) + " " + i + "," +
                      j, (int) CoorX(i), (int) CoorY(j));
            }
        }
    }
    
    public void setLimits(double xMin, double xMax, double yMin, double yMax, GammaPentagonal gamma) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.table = gamma.initializeTable( (int) yMax, (int) xMax);
        this.setPreferredSize(new Dimension(40 * (int) Math.abs(xMin - xMax),
                                        20 * (int) Math.abs(yMin - yMax)));
    }

    
    public void permutateTable(int[] a, byte table[][]) {
        byte array[][] = new byte[table.length][];
        for (int i = 0; i < a.length; i++) {
            array[i] = table[a[i]];
        }
        this.table = array;
        repaint();
  }
}