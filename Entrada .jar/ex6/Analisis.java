/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

/**
 *
 * @author sebas
 */
public class Analisis {
    
    public File file= new File(getClass().getResource("/resource/diccionario.txt").getFile());
    public static Scanner scan;
    public static Vector<String> palabras=new Vector<String>();
    private int claveAffinK;
    private int claveAffinB;
    
    public Analisis() throws FileNotFoundException{
        scan=new Scanner(file);
        while(scan.hasNext())palabras.add(scan.next());
        
        System.out.println(""+palabras.toString());
    }
    
    public int[] contar(String cText){
        int[] c={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        for(int i=  0; i<cText.length(); i++){

            c[cText.charAt(i)-97]++;
        }

        return c;
    }
    
    public int selectMax(String[] texts) throws FileNotFoundException{
        String aux;
        int index=2,maxpoints=0,len=0,points;
        for(int i=0;i<texts.length;i++){
            aux=texts[i];
            points=0;
            for(int j=0;j<palabras.size();j++){
                len=aux.length();
                aux=aux.replace(palabras.get(j),"");
                if(aux.length()<len){
                    points+=len-aux.length();
                    if(i==0)System.out.println(aux);
                }
                //System.out.println(palabras.get(j));

                if(aux.length()==0)break;
            }

            if(points>maxpoints){ index=i; maxpoints=points;}

        }
        return index;
    }
    
    public String affin(String cText){

        int[] arrayInv = {1,3,5,7,9,11,15,17,19,21,23,25};
        int maximo=0;
        String linea= new String();
        String pText= new String();
        int[] rank= new int [312];

        String[] es = new String[312];
        Affine c;
        String aux= new String();
        int k = 0;
        for(int j= 0; j<26; j++){
            for(int i=1; i<26; i+=2){
                c= new Affine(cText);
                if(i==13)continue;
                aux=c.descifrar(i,j);
                es[k]=aux;
                System.out.println(es[k]);
                k++;
            }
        }
        
        try {
            maximo = selectMax(es);
            System.out.println(maximo/12+"   "+maximo%12);
            claveAffinB = maximo/12;
            claveAffinK = arrayInv[maximo%12];
        }catch(IOException e){
            System.out.println ("Error al leer");
        }
        return es[maximo];
    }
    
    public int getClaveAffinK() {
        return claveAffinK;
    }

    public int getClaveAffinB() {
        return claveAffinB;
    }
}
