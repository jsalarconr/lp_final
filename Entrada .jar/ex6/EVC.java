package control;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class EVC {
    
    private final int size =3;
    
    private String outputFile1Path;
    private String outputFile2Path;
    private String outputFile3Path;
    private String outputFile4Path;
    private String outputFile5Path;
    private String decryptedImgPath;

    public String getDecryptedImgPath() {
        return decryptedImgPath;
    }

    public void setDecryptedImgPath(String decryptedImgPath) {
        this.decryptedImgPath = decryptedImgPath;
    }
    
    public String getOutputFile1Path() {
        return outputFile1Path;
    }

    public void setOutputFile1Path(String outputFile1Path) {
        this.outputFile1Path = outputFile1Path;
    }

    public String getOutputFile2Path() {
        return outputFile2Path;
    }

    public void setOutputFile2Path(String outputFile2Path) {
        this.outputFile2Path = outputFile2Path;
    }

    public String getOutputFile3Path() {
        return outputFile3Path;
    }

    public void setOutputFile3Path(String outputFile3Path) {
        this.outputFile3Path = outputFile3Path;
    }

    public String getOutputFile4Path() {
        return outputFile4Path;
    }

    public void setOutputFile4Path(String outputFile4Path) {
        this.outputFile4Path = outputFile4Path;
    }

    public String getOutputFile5Path() {
        return outputFile5Path;
    }

    public void setOutputFile5Path(String outputFile5Path) {
        this.outputFile5Path = outputFile5Path;
    }
    
    
    
    public void encrypt(BufferedImage share1, BufferedImage share2, BufferedImage target) throws IOException {
        int height = target.getHeight();
        int width = target.getWidth();
        BufferedImage htS1 = new BufferedImage(width * size, height * size, BufferedImage.TYPE_INT_RGB);
        BufferedImage htS2 = new BufferedImage(width * size, height * size, BufferedImage.TYPE_INT_RGB);
        BufferedImage htTarget = new BufferedImage(width * size, height * size, BufferedImage.TYPE_INT_RGB);
        BufferedImage encryptedS1 = new BufferedImage(width * size, height * size, BufferedImage.TYPE_INT_RGB);
        BufferedImage encryptedS2 = new BufferedImage(width * size, height * size, BufferedImage.TYPE_INT_RGB);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                
                int share1RGB = share1.getRGB(x, y);
                int share2RGB = share2.getRGB(x, y);
                int targetRGB = target.getRGB(x, y);

                int x1 = x * size;
                int y1 = y * size;
                
                int ST = this.randomHalf(htTarget, x1, y1, targetRGB,0);
                int S1 = this.randomHalf(htS1, x1, y1, share1RGB,0);
                int S2 = this.randomHalf(htS2, x1, y1, share2RGB,0);
                
                int P11, P10, P01;
                
                P11=ST;
                P10=S1-ST;
                P01=S2-ST;
                
                ArrayList<int[]> list = new ArrayList<>();
                
                for (int i = 0; i < P11; i++) {
                    if(list.size()==size*size) break;
                    int [] a = new int [2];
                    a[0]=1;
                    a[1]=1;
                    list.add(a);
                }
                
                for (int i = 0; i < P10; i++) {
                    if(list.size()==size*size) break;
                    int [] a = new int [2];
                    a[0]=1;
                    a[1]=0;
                    list.add(a);
                }
                
                for (int i = 0; i < P01; i++) {
                    if(list.size()==size*size) break;
                    int [] a = new int [2];
                    a[0]=0;
                    a[1]=1;
                    list.add(a);
                }
                
                while (list.size()<size*size) {
                    int [] a = new int [2];
                    a[0]=0;
                    a[1]=0;
                    list.add(a);
                }
                
                Collections.shuffle(list);
                
                int auxX = 0;
                int auxY = 0;
                for (int[] i : list) {
                    int rgbS1 = i[0];
                    int rgbS2 = i[1];
                    encryptedS1.setRGB(x1 + auxX, y1 + auxY, new Color(rgbS1*255, rgbS1*255, rgbS1*255).getRGB());
                    encryptedS2.setRGB(x1 + auxX, y1 + auxY, new Color(rgbS2*255, rgbS2*255, rgbS2*255).getRGB());
                    auxX++;
                    if (auxX == size) {
                        auxX = 0;
                        auxY++;
                    }
                }

            }
        }
        File output1 = File.createTempFile("encryptShare1","png");
        this.outputFile1Path = output1.getAbsolutePath();
        File output2 = File.createTempFile("encryptShare2","png");
        this.outputFile2Path = output2.getAbsolutePath();
        File output3 = File.createTempFile("halfToneS1","png");
        this.outputFile3Path = output3.getAbsolutePath();
        File output4 = File.createTempFile("halfToneS2","png");
        this.outputFile4Path = output4.getAbsolutePath();
        File output5 = File.createTempFile("halfToneTarget","png");
        this.outputFile5Path = output5.getAbsolutePath();
        try {
            ImageIO.write(encryptedS1, "png", output1);
            ImageIO.write(encryptedS2, "png", output2);
            ImageIO.write(htS1, "png", output3);
            ImageIO.write(htS2, "png", output4);
            ImageIO.write(htTarget, "png", output5);            

        } catch (IOException ex) {
            Logger.getLogger(EVC.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void decrypt(BufferedImage share1, BufferedImage share2) throws IOException{
        int width1 = share1.getWidth();
        int height1 = share1.getHeight();
        BufferedImage outputImage = new BufferedImage(width1, height1, BufferedImage.TYPE_INT_RGB);
        for(int y=0; y<height1; y++){
            for(int x=0; x<width1; x++){
                int c1 = new Color(share1.getRGB(x, y)).getBlue();
                int c2 = new Color(share2.getRGB(x, y)).getBlue();
                int c3;
                if(c1==0||c2==0) c3=0;
                else c3=255;
                outputImage.setRGB(x, y, new Color(c3,c3,c3).getRGB());
            }
        }
        File outputFile = File.createTempFile("decryptImage","png");
        this.decryptedImgPath = outputFile.getAbsolutePath();
        ImageIO.write(outputImage, "png", outputFile);
    }

    private int randomHalf(BufferedImage image, int x, int y, int rgb, int machete) {
        Color c = new Color(rgb);
        double grayLevel = ((c.getRed() + c.getGreen() + c.getBlue()) / 3)+machete;
        if(grayLevel<0) grayLevel = 0;
        if(grayLevel>255) grayLevel = 255;
        grayLevel = grayLevel / 255;
        int gray = (int) Math.round(grayLevel * size*size);
        
        ArrayList<Integer> list = new ArrayList<>();
        
        for(int i=0; i<gray; i++){
            list.add(1);
        }
        while(list.size()<size*size){
            list.add(0);
        }
        Collections.shuffle(list);
        int auxX = 0;
        int auxY = 0;
        for (Integer i : list) {
            int rgbS1 = i;
            image.setRGB(x + auxX, y + auxY, new Color(rgbS1*255, rgbS1*255, rgbS1*255).getRGB());
            auxX++;
            if (auxX == size) {
                auxX = 0;
                auxY++;
            }
        }
        return gray;
    }
    
    private int linearHalfFour(BufferedImage image, int x, int y, int rgb) {
        Color white = new Color(255, 255, 255);
        Color black = new Color(0, 0, 0);
        Color c = new Color(rgb);
        double grayLevel = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
        grayLevel = grayLevel / 255;
        int gray = (int) (grayLevel * 16);

        switch (gray) {
            case 16:
                image.setRGB(x, y, white.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, white.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, white.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, white.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 15:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, white.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, white.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, white.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 14:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, white.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, white.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 13:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, white.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 12:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 11:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 10:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;
            
            case 9:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, white.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 8:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 7:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 6:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 5:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, white.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 4:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, black.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 3:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, black.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 2:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, black.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                image.setRGB(x + 2, y + 3, white.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, black.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 1:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, black.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                image.setRGB(x + 2, y + 3, black.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, black.getRGB());
                image.setRGB(x + 3, y + 3, white.getRGB());
                break;

            case 0:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x, y + 3, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 1, y + 3, black.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                image.setRGB(x + 2, y + 3, black.getRGB());
                image.setRGB(x + 3, y, black.getRGB());
                image.setRGB(x + 3, y + 1, black.getRGB());
                image.setRGB(x + 3, y + 2, black.getRGB());
                image.setRGB(x + 3, y + 3, black.getRGB());
                break;
        }
        return gray;
    }
    
    private int linearHalf(BufferedImage image, int x, int y, int rgb) {
        Color white = new Color(255, 255, 255);
        Color black = new Color(0, 0, 0);
        Color c = new Color(rgb);
        double grayLevel = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
        grayLevel = grayLevel / 255;
        int gray = (int) (grayLevel * 9);

        switch (gray) {
            case 9:
                image.setRGB(x, y, white.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, white.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, white.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 8:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, white.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, white.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 7:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, white.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 6:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, white.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 5:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, white.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 4:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, white.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 3:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, white.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 2:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, white.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 1:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, white.getRGB());
                break;

            case 0:
                image.setRGB(x, y, black.getRGB());
                image.setRGB(x, y + 1, black.getRGB());
                image.setRGB(x, y + 2, black.getRGB());
                image.setRGB(x + 1, y, black.getRGB());
                image.setRGB(x + 1, y + 1, black.getRGB());
                image.setRGB(x + 1, y + 2, black.getRGB());
                image.setRGB(x + 2, y, black.getRGB());
                image.setRGB(x + 2, y + 1, black.getRGB());
                image.setRGB(x + 2, y + 2, black.getRGB());
                break;
        }
        return gray;
    }

    public int maskHalf(BufferedImage img, int x, int y, int rgb){
        
        int[][] boxmask = {{138, 230, 5, 219, 107},
            {87, 46, 179, 67, 148},
            {189, 26, 250, 15, 199},
            {158, 77, 168, 56, 97},
            {128, 209, 36, 240, 117}};
        Color white = new Color(255, 255, 255);
        Color black = new Color(0, 0, 0);
        Color c = new Color(rgb);
        double grayLevel = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
        int gray = (int) (grayLevel);
        System.out.println(gray);
        grayLevel = grayLevel / 255;
        int gray1 = (int) (grayLevel * 25);
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                if(gray >= boxmask[i][j]){
                    img.setRGB(i + x, j + y, black.getRGB());
                }else{
                    img.setRGB(i + x, j + y, white.getRGB());
                }
            }
        }
        return gray1;
    }

}
