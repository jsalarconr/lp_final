# LÉEME #

Este es el Proyecto Final de la asignatura Lenguajes de Programación 2015-2. 

El proyecto consiste en un traductor de dos vías entre lenguaje Java y Diagramas de Clases .


## ¿Para qué es este repositorio? ##

* Alojar el código escrito por el autor del mismo
* Alojar los ejemplos con los cuales puede ser usada la herramienta
* Instrucciones de instalación y uso

## ¿Como hago la instalación? ##

### Resumen ###

La aplicación consta de dos partes. 

La primera de ellas está desarrollada en lenguaje de servidor NodeJS y en el front tiene componentes de JavaScript, JQuery, HTML y CSS.

En resumen, para usar la aplicación se debe tener instalado nodejs en su ordenador, así como el administrador de paquetes npm (Node Package Manager).

La segunda, está desarrolla en Java. Para ejecutarla basta con hacer doble click sobre el archivo .jar que se encuentra sobre la carpeta apps.

A continuación se indicará paso a paso cómo debe ser la configuración y ejecución de la aplicación.

### Clonar el repositorio ###
Lo primero que debe hacer es clonar el repositorio, para ello introduzca el siguiente texto en una terminal:

```
#!git_commands

git clone https://jsalarconr@bitbucket.org/jsalarconr/lp_final.git
```

### Instalación de Node ###
Si usted está bajo una distribución Linux como Ubuntu, basta con ejecutar los siguientes comandos para instalar Node en su ordenador:

```
#!linux_commands

sudo apt-get update
sudo apt-get install nodejs
```
Acto seguido, deberá instalar npm para instalar los paquetes necesarios para ejecutar la aplicación:

```
#!linux_commands

sudo apt-get install npm
```
Para ver una guía más avanzada puede hacer click [aquí](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server)


### Dependencias ###
Una vez instalado *node* y *npm*, deberá instalar todos los paquetes necesarios para ejecutar el proyecto. Para ello, vaya a la carpeta clonada del repositorio y escriba:

```
#!linux_commands

sudo npm install
```
Espere a que termine el proceso y estará listo para ejecutar el proyecto.

### Ejecutar el proyecto ###
Una vez instalados los paquetes necesarios, estando situado en la carpeta clonada del repositorio, escriba:

```
#!linux_commands

node bin/www
```
El servidor se estará ejecutando en este momento. Acceda a la url: localhost:3000 en un navegador para ver la aplicación corriendo.

## Guía de funcionamiento ##

* Al acceder a la aplicación a través del navegador, encontrará una página de inicio. Allí se verán las principales características del programa
* Para traducir diagramas de clases, vaya a la sección "Parser" allí encontrará unos diagramas predeterminados los cuales podrá traducir y descargar una vez se finalice la traducción
* Para traducir lenguaje en Java, ejecute la segunda aplicación (archivo .jar) y allí ubique el directorio donde tiene las clases. La aplicación generará tres archivos de salida (librería Go.js, archivo classDiagram.js y plantilla en .html) que deberá poner juntos para poder ser visualizados.
* Bajo la carpeta "generated_files" se guardan los archivos generados por el programa de node.

## ¿Con quién contactar? ##

* Sebastián Alarcón. jsalarconr@unal.edu.co
